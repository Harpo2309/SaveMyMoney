<%-- 
    Document   : validaSesion
    Created on : 25/11/2016, 11:35:37 AM
    Author     : Harpo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%String context = request.getContextPath();%>

<s:if test="#session.logged != 'yes'">
    <script>
        window.location.replace("<%=context%>/index.jsp");
    </script>
</s:if>