<%-- 
    Document   : Register-step1
    Created on : 10/11/2016, 12:36:46 PM
    Author     : Harpo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

        <!--SWEET ALERT-->
        <script src="<%=context%>/sweetalert-master/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=context%>/sweetalert-master/dist/sweetalert.css">

        <!--HOJAS DE ESTILO -->
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/registerStyle.css"/>


        <!--
        JS PARA FUNCIONES
        -->
        <script type="text/javascript">

            function back() {                
                document.getElementById('back').href = "<%=context%>/index.jsp"
            }



        </script>
        <title>SaveMyMoney :: Crear una cuenta - Paso 1</title>
    </head>
    <body class="hold-transition login-page" style="background-image: url(<%=context%>/IMG/dregister/fondoblanco.jpg); position: center; background-size: contain">
        <div class="container">
            <div class="row" id="topBanner">

            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6" id="container-land">
                    <img class="img-responsive" id="banner" style="display: block" src="<%=context%>/IMG/register/step1-land.png"/>
                </div>
                <div class="col-md-6 col-sm-6" id="mainCol">
                    <div class="login-box" style="background-color: white;">
                        <div class="login-box-body" style="margin-top: -30px;">
                            <div class="login-logo" style="background-color: white;">
                                <img class="img-responsive" src="<%=context%>/IMG/logoai.png"/>
                            </div>
                            <form id="loginForm" name="login" action="inicioCrearCuenta" method="post">
                                <div class="form-group has-feedback">
                                    <p>Nombre:</p>
                                    <input type="text" required="" title="No uses números o símbolos"
                                           maxlength="45" pattern="^[a-zA-Z][a-zA-Záéíóú]{3,44}$"
                                           class="form-control" name="nuevoUsuario.nombre"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Primer apellido:</p>
                                    <input type="text" required="" title="No uses números o símbolos"
                                           maxlength="45" pattern="^[a-zA-Z][a-zA-Záéíóú]{3,44}$"
                                           class="form-control" name="nuevoUsuario.apellidoPaterno"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Segundo apellido:</p>
                                    <input type="text" required="" title="No uses números o símbolos"
                                           maxlength="45" pattern="^[a-zA-Z][a-zA-Záéíóú]{3,44}$"
                                           class="form-control" name="nuevoUsuario.apellidoMaterno"/>
                                </div>
                                <div class="row" align="center">
                                    <div class="col-md-6">
                                        <a id="back" onclick="back()" class="btn btn-link">atrás</a>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" value="Siguiente" class="btn btn-primary" style="background-color: yellowgreen;border-color: yellowgreen;"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
