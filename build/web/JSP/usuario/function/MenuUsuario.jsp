<%-- 
    Document   : MenuUSuario
    Created on : 10/11/2016, 12:37:22 PM
    Author     : Harpo
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/skins/_all-skins.min.css">
        <!-- AdminLTE Gráficas -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>




        <!-- CSS CREADOS POR CODELAB-->
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/customStyle.css"/>
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/StyleImg.css">
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/ImageSelect.css">
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/chosen.css">

        <script type="text/javascript">
            //muestra la imagen seleccionada
            function ShowSelected()
            {
            /* Para obtener el valor */
            var imagen = document.getElementById("sel").value;
            document.getElementById('mg').src = "IMG/Users/Login/" + imagen + ".jpg";
            }

            function walletName(idCartera) {
            var wName = document.getElementById('nombreCartera' + idCartera).value;
            var saldo = document.getElementById('saldoCartera' + idCartera).value;
            document.getElementById('walletName').innerText = wName;
            document.getElementById('saldoCar').value = saldo;
            document.getElementById('idCarAb').value = document.getElementById('id' + idCartera).value;
            document.getElementById('idTCCarAb').value = document.getElementById('idTC' + idCartera).value;
            document.getElementById('crediMax').value = document.getElementById('creditoMaxCartera' + idCartera).value;
            }

            function validAbonarCartera() {
            var saldo = parseInt(document.getElementById('saldoCar').value);
            var abono = parseInt(document.getElementById('abonoCartera').value);
            var tipoCartera = document.getElementById('idTCCarAb').value;
            var max = document.getElementById('crediMax').value;
            var parcial = (abono + saldo);
            if (tipoCartera === '2') {
            return true;
            } else if (parcial > max) {
            document.getElementById('abonoCartera').value = '';
            document.getElementById('abonoCartera').focus();
            swal('El abono excedería el crédito máximo', '(Crédito máximo: $ ' + max + ')');
            return false;
            } else {
            return true;
            }
            return true;
            }

            function valirTransaccion() {

            var nombreOrigen = $('input:radio[name=radioEnvia]:checked').data("nombre");
            var nombreDestino = $('input:radio[name=radioRecibe]:checked').data("nombre");
            var idOrigen = $('input:radio[name=radioEnvia]:checked').data("id");
            var saldo = $('input:radio[name=radioEnvia]:checked').data("saldo");
            var idDestino = $('input:radio[name=radioRecibe]:checked').data("id");
            if (nombreOrigen != null) {

            if (nombreDestino != null) {

            if (nombreDestino != nombreOrigen) {
            $("#nCarteraEnviada").html(nombreOrigen);
            $("#nCarteraRecibe").html(nombreDestino);
            $("#idCarteraEnviada").val(idOrigen);
            $("#idCarteraRecibe").val(idDestino);
            $("#saldoCarteraOrigen").val(saldo);
            $('#prepararTransaccion').modal('show');
            } else {
            swal("Las carteras son iguales");
            }
            } else {
            swal("selecciona la cartera de destino");
            }
            } else {
            swal("Selecciona la cartera de origen");
            }

            }

            function validarSaldoTransaccion() {
            var saldoActual = parseInt(document.getElementById('saldoCarteraOrigen').value);
            var saldoARestar = parseInt(document.getElementById('montoTransaccion').value);
            if (saldoARestar > saldoActual) {
            swal("Ingrese un monto inferior");
            $('#prepararTransaccion').modal('show');
            return false;
            } else {
            return true;
            }
            }

            function validarGasto() {
            var montoGasto = document.getElementById('montoGasto').value * 1.0;
            var montoCarteraMaximo = document.getElementById('saldoCarteraGasto').value * 1.0;
            if (montoGasto <= montoCarteraMaximo) {
            return true;
            } else {
            swal('Saldo insuficiente', ('Saldo actual de la cartera: $ ' + montoCarteraMaximo));
            return false;
            }
            return true;
            }

            function modificarCategoria(idCategoria, nombreCategoria) {
            document.getElementById('idCategoria').value = idCategoria;
            document.getElementById('nombreCat').value = nombreCategoria;
            }

        </script>

        <%-- --%>

        <!-- JS DEL SELECT DE IMAGEN PARA USER -->
        <script src="JS/jquery-1.11.0.min.js"></script>
        <script src="JS/chosen.jquery.js"></script>
        <script src="JS/ImageSelect.jquery.js"></script>
        <%-- --%>

        <!--LIBRERÍAS SWEET ALERT-->
        <script src="<%=context%>/sweetalert-master/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=context%>/sweetalert-master/dist/sweetalert.css">

        <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.css" />




        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <title>(<s:property value="#session.name"/>) SaveMyMoney</title>
        <jsp:include page="../../../LAYOUT/validaSesion.jsp"/>
    </head>

    <body class="hold-transition skin-blue sidebar-mini" onload="hideSaldo();
            ShowSelected()">
        <div class="wrapper">

            <header class="main-header" >
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>SVM</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Save My Money</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<%=context%>/IMG/Users/Login/<s:property value="#session.img"/>.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs" ><s:property value="#session.name"/></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img href="#" data-toggle="modal"  data-target="#modificarImagen"
                                             src="<%=context%>/IMG/Users/Login/<s:property value="#session.img"/>.jpg"
                                             class="img-circle" alt="User Image" data-toggle="tooltip" title="Cambiar avatar" id="imgProf">
                                        <br>
                                        <span style="color: white"><s:property value="#session.name"/></span>
                                        <br>

                                    </li>
                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" data-toggle="modal" data-target="#modificarPerfil" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<%=context%>/cerrarSesion" class="btn btn-default btn-flat">Cerrar Sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <!--
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                            -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<%=context%>/IMG/Users/Login/<s:property value="#session.img"/>.jpg" class="img-circle" alt="User Image">
                        </div>
                        
                        <div class="pull-left info">
                            <b>Saldo crédito:</b> $<s:property value="totalCarCred"/> 
                        </div>
                        <br>
                        <div class="pull-left info">
                            <b>Saldo otros:</b> $<s:property value="totalCarOtro"/> 
                        </div>
                        <br>
                        <div class="pull-left info">
                            <b>Total:</b> $<s:property value="totalCar"/> 
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">

                        <li role="presentation" class="active">
                            <a href="#mainContent" aria-controls="mainContent" role="tab" data-toggle="tab">
                                <i class="fa fa-th"></i> <span>Administrar carteras</span>
                            </a>
                        </li>
                        <li>
                        <li role="presentation">
                            <a href="#categorias" aria-controls="stats" role="tab" data-toggle="tab">
                                <i class="fa fa-gears"></i> <span>Categorías</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#abonos" aria-controls="abonos" role="tab" data-toggle="tab">
                                <i class="fa  fa-bank"></i> <span>Abonos</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#gastos" aria-controls="gastos" role="tab" data-toggle="tab">
                                <i class="fa fa-dollar"></i> <span>Gastos</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#trans" aria-controls="trans" role="tab" data-toggle="tab">
                                <i class="fa fa-line-chart"></i> <span>Transferencias</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#stats" aria-controls="stats" role="tab" data-toggle="tab">
                                <i class="fa fa-pie-chart"></i> <span>Estadísticas</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">



                <!-- Content Header (Page header) -->
                <section class="content-header">

                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-3">


                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9 ">
                        <div class="nav-tabs-custom">
                            <div class="tab-content">



                                <!--inicia pestaña carteras-->
                                <div id="mainContent" role="tabpanel" class="tab-pane active">
                                    <h1>Administrar carteras</h1>
                                    <br>                                   
                                    <h3>Carteras de crédito</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th>Crédito Máximo</th>
                                                    <th colspan="2">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioCredito">
                                                    <tr>
                                                <input type="hidden" id="idMiCarterita<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <td>$ <s:property value="creditoMaximo"/></td>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modificarCartera" onclick="modificarCartera(<s:property value="idCartera"/>)">Modificar</a></td>
                                                <td><a data-id-cartera="<s:property value="idCartera"/>" href="#" data-toggle="tooltip" title="Eliminar cartera" class="btn btn-danger btnEliminar">&times;</a></td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>

                                    <br>

                                    <h3>Otras carteras</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th colspan="2">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioOtro">
                                                    <tr>
                                                <input type="hidden" id="idMiCarterita<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modificarCartera" onclick="modificarCartera(<s:property value="idCartera"/>)">Modificar</a></td>
                                                <td><a data-id-cartera="<s:property value="idCartera"/>" href="#" data-toggle="tooltip" title="Eliminar cartera" class="btn btn-danger btnEliminar">&times;</a></td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>



                                    <br>
                                    <div id="nuevaCartera">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#registroCartera" >Crear cartera</a>
                                    </div>
                                    <br>
                                </div>
                                <!--termina pestaña carteras-->

                                <!--Pestañas de abonos-->
                                <div id="abonos" role="tabpanel" class="tab-pane">
                                    <h1>Abonos</h1>
                                    <h3>Carteras de crédito</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th>Crédito Máximo</th>
                                                    <th colspan="2">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioCredito">
                                                    <tr>
                                                <input type="hidden" id="id<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <input type="hidden" id="NombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <td>$ <s:property value="creditoMaximo"/></td>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><button onclick="walletName(<s:property value="idCartera"/>)" class="btn btn-primary" data-toggle="modal" data-target="#abonarCartera" >Abonar</button></td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>

                                    <br>

                                    <h3>Otras carteras</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-responsive" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th colspan="2">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioOtro">
                                                    <tr>
                                                <input type="hidden" id="id<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><button onclick="walletName(<s:property value="idCartera"/>)" class="btn btn-primary" data-toggle="modal" data-target="#abonarCartera" >Abonar</button></td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--termina pestaña de abonos-->


                                <!--inicia pestaña de gastos-->
                                <div id="gastos" role="tabpanel" class="tab-pane">
                                    <h1>Gastos</h1>
                                    <h3>Detalles</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Descripcion</th>
                                                    <th>Monto</th>
                                                    <th>Fecha</th>
                                                    <th>Categoría</th>
                                                    <th>Cartera</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:iterator value="MisGastos" status="stet"> 
                                                    <tr>
                                                <input type="hidden" id="id<s:property value="unGasto.idGasto"/>" value="<s:property value="idGasto"/>"/>
                                                <td><s:property value="descripcion"/></td>
                                                <td><s:property value="monto"/></td>
                                                <td><s:property value="fecha"/></td>
                                                <td><s:property value="categoria.nombre"/></td>          
                                                <td><s:property value="cartera.nombre"/></td>
                                                <%!int estado;%>
                                                <s:set name="borrar" value="borrar"/>
                                                <s:if test="%{#borrar==1}">                          
                                                    <td><a data-id-gasto="<s:property value="idGasto"/>" href="#" data-toggle="tooltip" title="Eliminar gasto" class="btn btn-danger btnEliminarGasto">&times;</a></td>
                                                </s:if>
                                                <s:else>

                                                </s:else>
                                                </tr>
                                            </s:iterator>
                                            </stbody>
                                        </table>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<%=context%>/registrarGasto">
                                            <div id="nuevaCartera">
                                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#registroGasto" >Realizar gasto</a>
                                            </div>
                                        </form>
                                    </div>


                                    <h5>*Sólo se pueden eliminar gastos con más de 30 días de antigüedad</h5>
                                </div>
                                <!--termina pestaña de gastos -->


                                <!--inicia pestaña de categorías-->
                                <div id="categorias" role="tabpanel" class="tab-pane">
                                    <h1>Categorías</h1>
                                    <br>
                                    <h3>Categorías por defecto</h3>  
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:iterator value="categoriasDefecto" status="stet"> 
                                                    <tr>
                                                        <td><s:property value="nombre"/></td>              
                                                    </tr>
                                                </s:iterator>
                                                </stbody>
                                        </table>
                                    </div>
                                    <br>
                                    <h3>Mis categorías</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:iterator value="unasCategorias" status="stet"> 
                                                    <tr>
                                                        <td><s:property value="nombre"/></td>  
                                                        <td><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modificarCategoria" onclick="modificarCategoria(<s:property value="idCategoria"/>, '<s:property value="nombre"/>')">Modificar</a></td>
                                                        <td><a data-id-categoria="<s:property value="idCategoria"/>" href="#" data-toggle="tooltip" title="Eliminar categoría" class="btn btn-danger btnEliminarCategoria">&times;</a></td>
                                                    </tr>
                                                </s:iterator>
                                                </stbody>
                                        </table>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<%=context%>/registrarCategoria">
                                            <div id="nuevaCartera">
                                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#registroCategoria" >Crear categoría</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--termina pestaña categorías-->




                                <!--inicia pestaña de Transferencias-->
                                <div id="trans" role="tabpanel" class="tab-pane">
                                    <h1>Transferencias</h1>
                                    <h3>Cartera que envía</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-responsive" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th>Seleccionar</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioOtro">
                                                    <tr>
                                                <input type="hidden" id="id<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><div class="radio"><label><input type="radio" value="<s:property value="idCartera"/>" id="radioEnvia<s:property value="idCartera"/>" data-nombre="<s:property value="nombre"/>" data-id="<s:property value="idCartera"/>" data-saldo="<s:property value="saldo"/>" name="radioEnvia"/></div></td>
                                                <td><div class="radio"><label><input type="radio" hidden value="<s:property value="nombre"/>" id="nombreEnvia<s:property value="idCartera"/>" data-nombre="<s:property value="idCartera"/>" name="nombreEnvia"/></div></td>

                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>

                                    <h3>Cartera que recibe</h3>
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tableWallets">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Saldo</th>
                                                    <th>Seleccionar</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <s:iterator value="carterasUsuarioOtro">
                                                    <tr>
                                                <input type="hidden" id="id<s:property value="idCartera"/>" value="<s:property value="idCartera"/>"/>
                                                <input type="hidden" id="idTC<s:property value="idCartera"/>" value="<s:property value="idTipoCartera"/>"/>
                                                <td><s:property value="nombre"/></td>
                                                <input type="hidden" id="nombreCartera<s:property value="idCartera"/>" value="<s:property value="nombre"/>"/>
                                                <td>$ <s:property value="saldo"/></td>
                                                <input type="hidden" id="saldoCartera<s:property value="idCartera"/>" value="<s:property value="saldo"/>"/>
                                                <input type="hidden" id="creditoMaxCartera<s:property value="idCartera"/>" value="<s:property value="creditoMaximo"/>"/>
                                                <td><div class="radio"><label><input type="radio" id="radioRecibe<s:property value="idCartera"/>" value="<s:property value="idCartera"/>" data-nombre="<s:property value="nombre"/>" data-id="<s:property value="idCartera"/>" name="radioRecibe"/></div></td>
                                                <td><div class="radio"><label><input type="radio" hidden value="<s:property value="nombre"/>" id="nombreRecibe<s:property value="idCartera"/>" name="nombreRecibe"/></div></td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
                                    <button onclick="valirTransaccion()"  class="btn btn-primary" data-toggle="modal" data-target="#trprepararTransaccion"asaccionFormulario" >Preparar Transacción</button></td>
                                </div>
                                <!--termina pestaña Transferencias-->


                                <!--inicia pestaña estadisticas-->
                                <div id="stats" role="tabpanel" class="tab-pane">
                                    <h1>Estadísticas</h1>

                                    <!-- Gastos por categoría -->
                                    <div class="box box-primary" style="width: 80% !important; height: 20% !important;">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Gastos por categorías</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body">

                                            <canvas id="myChart" style="height: 30% !important;"></canvas>
                                            <script>
                                                var ctx = document.getElementById("myChart");
                                                var myChart = new Chart(ctx, {
                                                type: 'pie',
                                                        data: {
                                                        labels: [
                                                <s:iterator value="datosGrafica" status="stat">
                                                    <s:if test="#stat.last==true">
                                                        '<s:property value="nombreCategoria"/>'
                                                    </s:if>
                                                    <s:else>
                                                        '<s:property value="nombreCategoria"/>',
                                                    </s:else>

                                                </s:iterator>
                                                        ],
                                                                datasets: [{
                                                                label: '',
                                                                        data: [
                                                <s:iterator value="datosGrafica" status="stat">

                                                                        "<s:property value="totalGastos"/>",
                                                </s:iterator>
                                                                        ],
                                                                        backgroundColor: [
                                                                                'rgba(255, 99, 132, 0.2)',
                                                                                'rgba(54, 162, 235, 0.2)',
                                                                                'rgba(255, 206, 86, 0.2)',
                                                                                'rgba(75, 192, 192, 0.2)',
                                                                                'rgba(153, 102, 255, 0.2)'
                                                                        ],
                                                                        borderColor: [
                                                                                'rgba(255,99,132,1)',
                                                                                'rgba(54, 162, 235, 1)',
                                                                                'rgba(255, 206, 86, 1)',
                                                                                'rgba(75, 192, 192, 1)',
                                                                                'rgba(153, 102, 255, 1)',
                                                                                'rgba(255, 159, 64, 1)'
                                                                        ],
                                                                        borderWidth: 1
                                                                }]
                                                        },
                                                        options: {
                                                        scales: {
                                                        yAxes: [{
                                                        ticks: {
                                                        beginAtZero: true
                                                        }
                                                        }]
                                                        }
                                                        }
                                                });
                                            </script>
                                        </div>
                                    </div>

                                    <!-- Transferencias -->
                                    <div class="box box-primary" style="width: 80% !important; height: 20% !important;">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Transferencias</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th>Cartera Origen</th>
                                                            <th>Monto</th>
                                                            <th>Cartera Destino</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <s:iterator value="misTransacciones">
                                                            <tr>
                                                                <td><s:property value="fecha"/></td>
                                                                <td><s:property value="carteraDestino"/></td>
                                                                <td><s:property value="monto"/></td>
                                                                <td><s:property value="carteraOrigen"/></td>
                                                            </tr>
                                                        </s:iterator>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Gastos por categoría -->
                                    <div class="box box-primary" style="width: 80% !important; height: 20% !important;">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Reportes</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body">

                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <button type="button" class="btn btn-default"><a href="<%=context%>/reporteGastos" target="about_blank">Gastos</a></button>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <button type="button" class="btn btn-default"><a href="<%=context%>/reporteAbonos" target="about_blank">Abonos</a></button>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <button type="button" class="btn btn-default"><a href="<%=context%>/reporteTransacciones" target="about_blank">Transacciones</a></button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--termina pestaña estadisticas-->
                                </div>




                                <!--TERMINAN PESTAÑAS DE DE FUNCIONES-->




                                <!--FORMULARIOS DE MODIFICACIÓN Y REGISTRO-->

                                <!--inicia formulario para registrar una categoría-->
                                <div class="bd-example">
                                    <div class="modal fade" id="registroCategoria" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Crear categoría</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="<%=context%>/registrarCategoria" method="post"> 
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="form-control-label">Nombre</label>
                                                            <br>
                                                            <input name="unaCategoria.nombre" id="nombre" type="text"  class="form-control" required="" size="50"/>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Registrar categoría"/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario para registrar una categoría-->

                                <!--inicia formulario de modificación de categoria-->
                                <div class="bd-example">
                                    <div class="modal fade" id="modificarCategoria" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Modificar Categoría</h4>

                                                </div>
                                                <div class="modal-body">
                                                    <form action="<%=context%>/modificarCategoria">
                                                        <input type="hidden" id="idCategoria" name="idCategoria"/>
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="form-control-label">Nombre</label>
                                                            <br>
                                                            <input name="nombre" id="nombreCat" type="text"  class="form-control" required=""/>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Guardar Cambios"/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario modificacion de categoria-->




                                <!--inicia formulario para registrar un gasto-->
                                <div class="bd-example">
                                    <div class="modal fade" id="registroGasto" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Realizar gasto</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <!--<form action="<//%=context%>/registrarGasto" onsubmit="return validarSaldoCreditoCartera()">-->

                                                    <form action="<%=context%>/registrarGasto" onsubmit="return validarGasto()" method="post"> 
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="form-control-label">Descripción</label>
                                                            <br>
                                                            <input name="unGasto.descripcion" id="descripcionGasto" type="text"  class="form-control" required="" size="50"/>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Monto</label>
                                                            <br>
                                                            <input name="unGasto.monto"  id="montoGasto" type="number" step="any" min="0" class="form-control" required=""/>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Fecha</label>
                                                            <br>
                                                            <input name="unGasto.fecha"  value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>" id="fechaGasto" type="date" step="any" min="0" class="form-control" required=""/>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Categoría</label>
                                                            <br>
                                                            <select id="idCategoriaGasto" name="unGasto.categoria.idCategoria" class="form-control">
                                                                <s:iterator value="misCategorias">
                                                                    <option value="<s:property value="idCategoria"/>"><s:property value="nombre"/></option>
                                                                </s:iterator>
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Cartera</label>
                                                            <br>
                                                            <select id="idCarteraGasto" name="unGasto.cartera.idCartera" class="form-control">
                                                                <s:iterator value="misCarteras">
                                                                    <option value="<s:property value="idCartera"/>"><s:property value="nombre"/></option>
                                                                    <option id="saldoCartera" hidden="" value="<s:property value="saldo"/>"></option>
                                                                </s:iterator>            
                                                            </select>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Registrar gasto"/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario para registrar un gasto-->


                                <!--inicia formulario de modificación de cartera-->
                                <div class="bd-example">
                                    <div class="modal fade" id="modificarCartera" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Modificar Cartera</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form action="<%=context%>/modificarCartera" onsubmit="return validModifCartera()">
                                                        <div class="form-group">
                                                            <input type="hidden" name="cartera.idCartera" id="idMiCarterita"/>
                                                            <input type="hidden" name="cartera.idTipoCartera" id="idTC"/>
                                                            <label for="recipient-name" class="form-control-label">Nombre</label>
                                                            <br>
                                                            <input name="cartera.nombre" id="nuevoNombreCartera" type="text"  class="form-control" required=""/>
                                                        </div>
                                                        <div class="form-group" id="saldoContenedor">
                                                            <label for="message-text" class="form-control-label">Saldo</label>
                                                            <br>
                                                            <input name="cartera.saldo"  id="nuevoSaldoCartera" type="tel"  class="form-control" required=""/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label id="nuevoCreditoMaxCarteraLabel" for="message-text" class="form-control-label">Crédito Máximo</label>
                                                            <br>
                                                            <input name="cartera.creditoMaximo" id="nuevoCreditoMaxCartera"
                                                                   type="number" step="any" max="999999" class="form-control" required=""/>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Guardar Cambios"/>
                                                        </div>
                                                    </form>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario modificacion de cartera-->


                                <!--inicia formulario de creación de cartera-->
                                <div class="bd-example">
                                    <div class="modal fade" id="registroCartera" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Crear Cartera</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form action="<%=context%>/crearCartera" onsubmit="return validarSaldoCreditoCartera()">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="form-control-label">Nombre</label>
                                                            <br>
                                                            <input name="cartera.nombre" id="nuevoNombreCartera" type="text"  class="form-control" required=""/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Saldo</label>
                                                            <br>
                                                            <input name="cartera.saldo"  id="saldoBox" type="number" step="any" min="0" class="form-control" required=""/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Tipo de cartera</label>
                                                            <br>
                                                            <select onchange="crearCarteraFields()" id="lista" name="cartera.idTipoCartera" class="form-control">
                                                                <s:iterator value="tiposCartera">
                                                                    <option value="<s:property value="idTipoCartera"/>"><s:property value="nombre"/></option>
                                                                </s:iterator>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label id="creditLabel" for="message-text" class="form-control-label">Crédito Máximo</label>
                                                            <br>
                                                            <input id="creditBox" name="cartera.creditoMaximo"  type="number" step="any" min="0" class="form-control" required=""/>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Registrar cartera"/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario-->


                                <!--inicia formulario para modificar perfil del usuario-->
                                <div class="bd-example">
                                    <div class="modal fade" id="modificarPerfil" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Editar perfil</h4>
                                                    <h5>Toca un apartado para editar tus datos</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="panel-group" id="accordion" align="center">
                                                        <form action="<%=context%>/modificarPerfil" onsubmit="return verifDatosPerfil()">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <input type="hidden" class="form-control" name="usuario.idUsuario" value="<s:property value="usuario.idUsuario"/>" required=""/>
                                                                    <h3>
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                                                            Nombre y apellidos
                                                                        </a>
                                                                    </h3>
                                                                </div>
                                                                <div id="collapse1" class="panel-collapse collapse out">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <label for="recipient-name" class="form-control-label">Nombre</label>
                                                                            <br>
                                                                            <input type="text" class="form-control" name="usuario.nombre" value="<s:property value="usuario.nombre"/>" required=""/>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <label for="recipient-name" class="form-control-label">Primer apellido</label>
                                                                                    <br>
                                                                                    <input type="text" class="form-control" name="usuario.apellidoPaterno" value="<s:property value="usuario.apellidoPaterno"/>" required=""/>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <label for="recipient-name" class="form-control-label">Segundo apellido</label>
                                                                                    <br>
                                                                                    <input type="text" class="form-control" name="usuario.apellidoMaterno" value="<s:property value="usuario.apellidoMaterno"/>" required=""/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h3>
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                                                            Correo
                                                                        </a>
                                                                    </h3>
                                                                </div>
                                                                <div id="collapse2" class="panel-collapse collapse out">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <label for="recipient-name" class="form-control-label">Correo</label>
                                                                            <br>
                                                                            <input type="mail" class="form-control" name="usuario.correo" value="<s:property value="usuario.correo"/>" required=""/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h3>
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                                                            Contraseña
                                                                        </a>
                                                                    </h3>
                                                                </div>
                                                                <div id="collapse3" class="panel-collapse collapse out">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <label for="recipient-name" class="form-control-label">Contraseña actual</label>
                                                                                    <br>
                                                                                    <input id="currentPass" type="hidden" class="form-control" name="usuario.password" value="<s:property value="usuario.password"/>" required=""/>
                                                                                    <input id="currentPassVerif" type="password" class="form-control"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <label for="recipient-name" class="form-control-label">Nueva contraseña</label>
                                                                                    <br>
                                                                                    <input id="newPass1" type="password" class="form-control"/>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <label for="recipient-name" class="form-control-label">Confirmar contraseña</label>
                                                                                    <br>
                                                                                    <input id="newPass2" type="password" class="form-control"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                <input type="submit" class="btn btn-primary" value="Guardar cambios"/>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario para modificar perfil del usuario-->

                                <!--inicia formulario para modificar Imagen del usuario-->
                                <div class="bd-example">
                                    <div class="modal fade" id="modificarImagen" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Cambiar imagen</h4>

                                                </div>
                                                <div class="modal-body">
                                                    <form action="<%=context%>/cambiarImagenPerfil">
                                                        <div class="container">
                                                            <br><br><br>
                                                            <select  id="sel" class="my-select form-control" onchange="ShowSelected();" name="imagen" >
                                                                <s:iterator value="misImagenes" >
                                                                    <option  value="<s:property value="idImagen"/>"  data-img-src="IMG/Users/Login/<s:property value="idImagen"/>.jpg" ><s:property value="nombre"/></option>
                                                                </s:iterator>
                                                            </select>
                                                            <script>
                                                                //toma la imagen seleccionada en el select de cambio de foto de perfil
                                                                $(".my-select").chosen({
                                                                width: "25%",
                                                                        html_template: '<img style="border:1px solid black;padding:0px;margin-right:5px " class="{class_name}" src="{url}">'
                                                                });
                                                            </script>
                                                            <div id="content">
                                                                <br>
                                                                <img  id="mg" class="img-circle" style="width: 200px; height: 200px;">
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <input type="submit" class="btn btn-primary" value="Cambiar avatar"/>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--termina formulario para modificar perfil del usuario-->


                                <!--inicia formulario abonar-->
                                <div class="bd-example">
                                    <div class="modal fade" id="abonarCartera" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="titulo">Abonar a la cartera <label id="walletName"></label></h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form action="<%=context%>/abonarCartera" onsubmit="return validAbonarCartera()">
                                                        <div class="form-group">
                                                            <input type="hidden" name="abono.idCartera" id="idCarAb"/>
                                                            <input type="hidden" id="idTCCarAb"/>
                                                            <input type="hidden" id="saldoCar"/>
                                                            <input type="hidden" id="crediMax"/>
                                                            <input type="hidden" name="abono.idUser" id="idUserAb"  value="<s:property value="#session.id"/>"/>
                                                        </div>
                                                        <div class="form-group" id="saldoContenedor">
                                                            <label for="message-text" class="form-control-label">Cantidad</label>
                                                            <br>
                                                            <input name="abono.monto"  id="abonoCartera" type="number" min="1" max="999999" class="form-control" required=""/>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Descripción</label>
                                                            <br>
                                                            <textarea name="abono.descrip" id="descrip" class="form-control" required=""></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="message-text" class="form-control-label">Fecha</label>
                                                            <br>
                                                            <input type="date" value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>" name="abono.fecha" class="form-control date"/>
                                                        </div>
                                                </div>


                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                    <input type="submit" class="btn btn-primary" value="Guardar Cambios"/>
                                                </div>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--termina formulario abonar-->




                            <!--inicia formulario de transaccion-->
                            <div class="bd-example">
                                <div class="modal fade" name="prepararTransaccion" id="prepararTransaccion" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="titulo">Realizar transacción</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form name="formTrans" onsubmit="return validarSaldoTransaccion()" action="<%=context%>/registrarTransaccion">
                                                    <div class="form-group">
                                                        <h4>Cartera que envía: </h4>
                                                        <label id="nCarteraEnviada"></label>
                                                        <input id="idCarteraEnviada" hidden name="unaTransaccion.unaCarteraQuita.idCartera"/>
                                                        <input type="hidden" id="saldoCarteraOrigen"/>
                                                        <h4>Cartera que Recibe:</h4>
                                                        <label id="nCarteraRecibe"></label>
                                                        <input id="idCarteraRecibe" hidden name="unaTransaccion.unaCarteraRecibe.idCartera"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <h4> Selecciona el monto que deseas transferir</h4>
                                                        <input required="" class="form-control" type="number" id="montoTransaccion" name="unaTransaccion.monto" min="1"/>           
                                                    </div>
                                                    <div class="form-group">
                                                        <h4>Selecciona la fecha de la transacción</h4>
                                                        <input required="" class="form-control date" type="date" name="unaTransaccion.fecha" value="<%= new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>"/>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button  type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                        <input type="submit" class="btn btn-success" value="Realizar Transacción"/>
                                                    </div>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--termina formulario transaccion-->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>CodeLABS</b> 2016
        </div>
        <strong>Save My Money</strong> 
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                <p>New phone +1(800)555-1234</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                <p>nora@example.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                <p>Execution time 5 seconds</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Resume
                                <span class="label label-success pull-right">95%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Laravel Integration
                                <span class="label label-warning pull-right">50%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Back End Framework
                                <span class="label label-primary pull-right">68%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->

            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<%=context%>/AdminLTE-2.3.7/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script data-require="sweet-alert@*" data-semver="0.4.2" src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<%=context%>/AdminLTE-2.3.7/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<%=context%>/AdminLTE-2.3.7/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<%=context%>/AdminLTE-2.3.7/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<%=context%>/AdminLTE-2.3.7/dist/js/demo.js"></script>

<!--JS CREADOS POR CODELABS-->
<script type="text/javascript" src="<%=context%>/JS/customJS.js"></script>
<script type="text/javascript" src="<%=context%>/JS/sweetAlertFunctions.js"></script>


<!--Formulario para eliminar carteras-->
<form id="formElim" action="<%=context%>/eliminarCartera">
    <input type="hidden" id="idForm" name="idCartera"/>
</form>
<!--Formulario para eliminar gastos-->
<form id="formElimGasto" action="<%=context%>/eliminarGasto">
    <input type="hidden" id="idFormGasto" name="idGasto"/>
</form>

<!--Formulario para eliminar categorías-->
<form id="formElimCategoria" action="<%=context%>/eliminarCategoria">
    <input type="hidden" id="idFormCategoria" name="idCategoria"/>
</form>
</body>

</html>
