package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_if_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_property_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_s_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_property_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_s_if_test.release();
    _jspx_tagPool_s_property_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
String context = request.getContextPath();
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <!-- Tell the browser to be responsive to screen width -->\n");
      out.write("        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">\n");
      out.write("        <!-- Bootstrap 3.3.6 -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"");
      out.print(context);
      out.write("/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css\">\n");
      out.write("        <!-- Font Awesome -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css\">\n");
      out.write("        <!-- Ionicons -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css\">\n");
      out.write("        <!-- Theme style -->\n");
      out.write("        <link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"");
      out.print(context);
      out.write("/AdminLTE-2.3.7/dist/css/AdminLTE.min.css\">\n");
      out.write("        <!-- AdminLTE Skins. Choose a skin from the css/skins\n");
      out.write("             folder instead of downloading all of them to reduce the load. -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"");
      out.print(context);
      out.write("/AdminLTE-2.3.7/dist/css/skins/_all-skins.min.css\">\n");
      out.write("        <!-- <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">-->\n");
      out.write("        <title>Save My Money</title>\n");
      out.write("\n");
      out.write("        <!--este codigo hará que si la sesión está iniciada se redireccione al menu sin iniciar\n");
      out.write("        sesion de nuevo\n");
      out.write("        -->\n");
      out.write("\n");
      out.write("        ");
      //  s:if
      org.apache.struts2.views.jsp.IfTag _jspx_th_s_if_0 = (org.apache.struts2.views.jsp.IfTag) _jspx_tagPool_s_if_test.get(org.apache.struts2.views.jsp.IfTag.class);
      _jspx_th_s_if_0.setPageContext(_jspx_page_context);
      _jspx_th_s_if_0.setParent(null);
      _jspx_th_s_if_0.setTest("#session.logged == 'yes'");
      int _jspx_eval_s_if_0 = _jspx_th_s_if_0.doStartTag();
      if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
          out = _jspx_page_context.pushBody();
          _jspx_th_s_if_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
          _jspx_th_s_if_0.doInitBody();
        }
        do {
          out.write("\n");
          out.write("            <script>\n");
          out.write("                window.location.replace(\"");
          out.print(context);
          out.write("/miCuenta\");\n");
          out.write("            </script>\n");
          out.write("        ");
          int evalDoAfterBody = _jspx_th_s_if_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
        if (_jspx_eval_s_if_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
          out = _jspx_page_context.popBody();
      }
      if (_jspx_th_s_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_0);
        return;
      }
      _jspx_tagPool_s_if_test.reuse(_jspx_th_s_if_0);
      out.write("     \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body class=\"hold-transition login-page\" style=\"background-image: url(IMG/fondo_verde2.png); position: center; background-size: contain\">\n");
      out.write("        <div class=\"login-box\">\n");
      out.write("            <div class=\"login-box-body\" style=\"margin-top: -30px;\">\n");
      out.write("                <div class=\"login-logo\" style=\"background-color: white;\">\n");
      out.write("                    <img class=\"img-responsive\" src=\"IMG/logoai.png\" width=\"252\" height=\"110\" style=\"align-self:center\"/>\n");
      out.write("                </div>\n");
      out.write("                <p class=\"login-box-msg\"><strong>Iniciar sesión</strong></p>\n");
      out.write("                <p class=\"login-box-msg\"><strong>");
      if (_jspx_meth_s_property_0(_jspx_page_context))
        return;
      out.write("</strong></p>\n");
      out.write("                <form name=\"login\" action=\"loginUsuario\" method=\"post\">\n");
      out.write("                    <div class=\"form-group has-feedback\">\n");
      out.write("                        <p>Correo electrónico:</p>\n");
      out.write("                        <input required=\"\" type=\"email\" class=\"form-control\" name=\"correo\"/>\n");
      out.write("                        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group has-feedback\">\n");
      out.write("                        <p>Contraseña:</p>\n");
      out.write("                        <input required=\"\" type=\"password\" name=\"password\" class=\"form-control\"/>\n");
      out.write("                        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xs-4\">\n");
      out.write("                            <input type=\"submit\" value=\"Iniciar sesión\" class=\"btn btn-primary\" style=\"background-color: yellowgreen;border-color: yellowgreen;\"/>\n");
      out.write("                        </div></div>\n");
      out.write("                </form>\n");
      out.write("                <br>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 col-sm-6 col-xs-6\">\n");
      out.write("                        <a href=\"");
      out.print(context);
      out.write("/JSP/usuario/register/Register-step1.jsp\">Crear cuenta</a>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6 col-sm-6 col-xs-6\">\n");
      out.write("                        <a href=\"");
      out.print(context);
      out.write("/JSP/usuario/login/ForgotPassword.jsp\">¿Olvidaste tu contraseña?</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>         \n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"panel-footer\" style=\"background:#FFC414\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n");
      out.write("                        <div class=\"copyright\" style=\"color:white\">\n");
      out.write("                            <p>© 2016, CodeLab. Todos los derechos reservados. </p>\n");
      out.write("                            <p></p>\n");
      out.write("                            <p></p>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_s_property_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_0 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_0.setPageContext(_jspx_page_context);
    _jspx_th_s_property_0.setParent(null);
    _jspx_th_s_property_0.setValue("mensaje");
    int _jspx_eval_s_property_0 = _jspx_th_s_property_0.doStartTag();
    if (_jspx_th_s_property_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
    return false;
  }
}
