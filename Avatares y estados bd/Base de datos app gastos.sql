---ELABOR�: EQUIPO 4 - CODELABS
---SCRIPT DE LA BASE DE DATOS PARA LA APLICACI�N DE CONTROL DE GASTOS

--ELIMINAR LA BASE DE DATOS----------------- 
USE master;
GO
DROP DATABASE appGastos;
GO
--------------------------------------------


--COMIENZA CREACI�N DE LA BASE DE DATOS
CREATE DATABASE appGastos;
GO
USE appGastos;
GO

CREATE TABLE Imagen(
idImagen INT IDENTITY(1,1) NOT NULL,
nombre VARCHAR(45) NOT NULL,
CONSTRAINT pk_icono PRIMARY KEY(idImagen));
GO

CREATE TABLE Usuario(
idUsuario INT IDENTITY(1,1) NOT NULL,
nombre VARCHAR(45) NOT NULL,
apellidoP VARCHAR(45) NOT NULL,
apellidoM VARCHAR(45),
correo VARCHAR(150) NOT NULL UNIQUE,
contrasena VARCHAR(22) NOT NULL,
fotoPerfil int
CONSTRAINT pk_usuario PRIMARY KEY(idUsuario),
constraint fk_foto foreign key(fotoPerfil) references Imagen(idImagen));
GO


CREATE TABLE tipoCartera(
idTipoCartera INT NOT NULL IDENTITY(1,1),
nombre VARCHAR(45)NOT NULL,
CONSTRAINT pk_tipoCartera PRIMARY KEY(idTipoCartera));
GO

CREATE TABLE Cartera(
idCartera INT IDENTITY(1,1) NOT NULL,
nombre VARCHAR(45) NOT NULL,
saldo MONEY NOT NULL CHECK(saldo>=0) DEFAULT 0,
creditoMaximo MONEY CHECK(creditoMaximo>=0),
idUsuario INT NOT NULL,
idTipoCartera INT NOT NULL,
CONSTRAINT pk_cartera PRIMARY KEY(idCartera),
CONSTRAINT fk_cartera_usuario FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
CONSTRAINT fk_cartera_tipoCartera FOREIGN KEY(idTipoCartera) REFERENCES tipoCartera(idTipoCartera));
GO
--MOSTRAR TOTAL DE CARTERAS DE TIPO TARJETA DE CREDITO
;
GO
select * from Usuario 
--MOSTRAR TOTAL DE CARTERAS DE TIPO OTRO
SELECT SUM(saldo) AS totalCarteraOtro FROM Cartera GROUP BY idUsuario,idTipoCartera HAVING idUsuario=4 AND idTipoCartera=2;
GO

--MOSTRAR TOTAL DE TODAS LAS CARTERAS
SELECT SUM(saldo) AS totalCarteras FROM Cartera GROUP BY idUsuario HAVING idUsuario=3;
GO

--MOSTRAR TOTAL DE CARTERAS CREADAS
SELECT COUNT(*) AS numeroCarteras FROM Cartera GROUP BY idUsuario HAVING idUsuario=3;

--MOSTRAR TOTAL DE GASTOS CREADAS
SELECT COUNT(*) AS numeroGastos FROM Gasto GROUP BY idUsuario HAVING idUsuario=3;



CREATE TABLE Categoria( 
idCategoria INT IDENTITY(1,1) NOT NULL,
nombre VARCHAR(45) NOT NULL,
predefinida BIT NOT NULL DEFAULT 0,
idUsuario INT NOT NULL
CONSTRAINT pk_categoria PRIMARY KEY(idCategoria),
CONSTRAINT fk_categoria_usuario FOREIGN KEY(idUsuario)REFERENCES Usuario(idUsuario)
);
GO

CREATE TABLE Predeterminada( 
idPredeterminada INT IDENTITY(1,1) NOT NULL,
nombre VARCHAR(45) NOT NULL UNIQUE,
predeterminada BIT NOT NULL DEFAULT 1,
CONSTRAINT pk_predeterminada PRIMARY KEY(idPredeterminada)
);
GO

CREATE TABLE Gasto(
idGasto INT IDENTITY(1,1) NOT NULL,
descripcion VARCHAR(300) NOT NULL,
monto MONEY NOT NULL CHECK(monto>0),
fecha DATE DEFAULT GETDATE(),
idUsuario INT NOT NULL,
idCategoria INT NOT NULL,
idCartera INT NOT NULL,
borrar BIT DEFAULT 0,
CONSTRAINT pk_gasto PRIMARY KEY(idGasto),
CONSTRAINT fk_gasto_usuario FOREIGN KEY(idUsuario)REFERENCES Usuario(idUsuario),
CONSTRAINT fk_gasto_categoria FOREIGN KEY(idCategoria)REFERENCES Categoria(idCategoria),
CONSTRAINT fk_gasto_cartera FOREIGN KEY(idCartera)REFERENCES Cartera(idCartera));
GO

CREATE TABLE Ingreso(
idIngreso INT IDENTITY(1,1) NOT NULL,
descripcion VARCHAR(45) NOT NULL,
monto MONEY CHECK(monto>0) NOT NULL,
fecha DATE DEFAULT GETDATE() NOT NULL,
idCartera INT NOT NULL,
idUsuario INT NOT NULL,
CONSTRAINT pk_ingreso PRIMARY KEY(idIngreso),
CONSTRAINT fk_ingreso_cartera FOREIGN KEY(idCartera)REFERENCES Cartera(idCartera),
CONSTRAINT fk_ingreso_usuario FOREIGN KEY(idUsuario)REFERENCES Usuario(idUsuario));
GO

CREATE TABLE Transferencia(
idTransferencia INT IDENTITY(1,1) NOT NULL,
monto MONEY CHECK(monto>0) NOT NULL,
fecha DATE DEFAULT GETDATE()NOT NULL,
idCarteraQuita INT NOT NULL,
idCartera INT NOT NULL,
idUsuario INT NOT NULL,
CONSTRAINT pk_transferencia PRIMARY KEY(idTransferencia),
CONSTRAINT fk_transferencia_carteraQuita FOREIGN KEY(idCarteraQuita)REFERENCES Cartera(idCartera),
CONSTRAINT fk_transferencia_cartera FOREIGN KEY(idCartera)REFERENCES Cartera(idCartera),
CONSTRAINT fk_transferencia_usuario FOREIGN KEY(idUsuario)REFERENCES Usuario(idUsuario));
GO

CREATE TABLE Deuda(
idDeuda INT IDENTITY(1,1) NOT NULL,
descripcion VARCHAR(100) NOT NULL,
monto MONEY CHECK(monto>0) NOT NULL,
idUsuario INT NOT NULL,
CONSTRAINT pk_deuda PRIMARY KEY(idDeuda),
CONSTRAINT fk_deuda_usuario FOREIGN KEY(idUsuario)REFERENCES Usuario(idUsuario));
GO
--FINALIZA CREACI�N DE LA BASE DE DATOS

--COMIENZA INSERCI�N DE DATOS


INSERT INTO Imagen(nombre) values('Caballero');
INSERT INTO Imagen(nombre) values('Monsieur');
INSERT INTO Imagen(nombre) values('Pirata');
INSERT INTO Imagen(nombre) values('Dama');
INSERT INTO Imagen(nombre) values('Chico');
INSERT INTO Imagen(nombre) values('Chica');
INSERT INTO Imagen(nombre) values('Miss');
INSERT INTO Imagen(nombre) values('Baby');
GO



INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	VALUES ('Sarahi Daniela','Medina','Martinez','penlove87@hotmail.com','123456789', 1);
			
INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	VALUES ('Celica','Cabrera','S�nchez','celica@gmail.com','2223335896', 2);
			
INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	VALUES ('Francisco','Gardu�o','Estrada','francisco@gmail.com','12569863', 3);

INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	VALUES ('Ricardo Lino','Garc�a','L�pez','ricardo@gmail.com','7788999636', 2);

INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	VALUES ('Giovanni','Garc�a','Ferrari','giovanni@gmail.com','558899', 4);
GO


INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Servicios',1,1);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Abarrotes',1,1);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Servicios',1,2);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Abarrotes',1,2);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Combustible',0,2);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Servicios',1,3);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Abarrotes',1,3);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Comida',0,3);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Servicios',1,4);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Abarrotes',1,4);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Servicios',1,5);
INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES('Abarrotes',1,5);
GO

INSERT INTO Predeterminada(nombre) VALUES('Servicios');
INSERT INTO Predeterminada(nombre) VALUES('Abarrotes');
GO

INSERT INTO tipoCartera(nombre)VALUES('Tarjeta de cr�dito');
INSERT INTO tipoCartera(nombre)VALUES('Otro');
GO

INSERT INTO Cartera(nombre,saldo,idUsuario,idTipoCartera)VALUES('Imprevistos',2000.00,1,2);
INSERT INTO Cartera(nombre,saldo,creditoMaximo,idUsuario,idTipoCartera)VALUES('Tarjeta',150000.00,300000.00,1,1);
INSERT INTO Cartera(nombre,saldo,creditoMaximo,idUsuario,idTipoCartera)VALUES('Bancomer',35850.60,100000.00,2,1);
INSERT INTO Cartera(nombre,saldo,idUsuario,idTipoCartera)VALUES('Guardadito',18950.50,2,2);
INSERT INTO Cartera(nombre,saldo,creditoMaximo,idUsuario,idTipoCartera)VALUES('Banamex',24850.00,38579.50,3,1);
INSERT INTO Cartera(nombre,saldo,idUsuario,idTipoCartera)VALUES('Quincena',12856.10,3,2);
GO

INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('15 litros de gasolina',280.50,'2016-10-01',3,6,3);
INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('Pago recibo CFE',370.00,'2016-10-01',1,1,1);
INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('Despensa Walmart',1095.00,'2016-10-05',2,4,2);
INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('Comida en VIPS',198.00,'2016-10-10',4,5,3);
INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('Pago �zzi!',560.00,'2016-10-11',1,2,1);
INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)
VALUES('10 litros gasolina',148.50,'2016-10-12',3,3,2);
GO

INSERT INTO Ingreso(descripcion,monto,fecha,idCartera,idUsuario)VALUES('Tanda',1000.00,'2016-10-12',4,2);
INSERT INTO Ingreso(descripcion,monto,fecha,idCartera,idUsuario)VALUES('Regalo cumplea�os',250.00,'2016-10-12',5,3);
INSERT INTO Ingreso(descripcion,monto,fecha,idCartera,idUsuario)VALUES('Premio puntualidad',150.00,'2016-10-13',1,1);
GO

INSERT INTO Transferencia(monto,fecha,idCarteraQuita,idCartera,idUsuario)VALUES(2500.00,'2016-10-14',3,4,2);
INSERT INTO Transferencia(monto,fecha,idCarteraQuita,idCartera,idUsuario)VALUES(8000.00,'2016-10-14',6,5,3);
INSERT INTO Transferencia(monto,fecha,idCarteraQuita,idCartera,idUsuario)VALUES(2000.00,'2016-10-14',2,1,1);
GO

------FINALIZA INSERCCI�N DE DATOS-------------------------------------------






--COMIENZA CREACI�N DE PROCEDIMIENTO ALMACENADOS PARA USUARIO

CREATE PROCEDURE pa_insertarUsuario
---ESTE PROCEDIMIENTO ALMACENADO INSERTA UN NUEVO USUARIO AL SISTEMA, ADEMAS SE LE ASIGNAN LAS CATEGORIAS POR DEFECTO
@nombre VARCHAR(45),
@apellidoP VARCHAR(45),
@apellidoM VARCHAR(45),
@correo VARCHAR(150),
@contrasena VARCHAR(22),
@fotoPerfil int
AS
BEGIN
   DECLARE @idUsuario INT;
   DECLARE @i INT;
   DECLARE @idUltimo INT;
   INSERT INTO Usuario(nombre,apellidoP	,apellidoM,correo,contrasena,fotoPerfil)
   VALUES(@nombre,@apellidoP,@apellidoM,@correo,@contrasena,@fotoPerfil);
   SELECT @idUsuario=@@IDENTITY;
   PRINT 'EL ID DEL USUARIO REGISTRADO ES '+CONVERT(VARCHAR,@idUsuario);
   ---OBTENER LA ULTIMA LLAVE FORANEA DE LA TABLA PREDETERMINADA
   SELECT TOP 1 @idUltimo=idPredeterminada FROM Predeterminada ORDER BY idPredeterminada DESC;
   ---RECORRER LAS TUPLAS DE LA TABLA
   SET @i=1;
       DECLARE @nombreP VARCHAR(45);
       DECLARE @predefinida BIT;
       DECLARE @existe BIT;
   WHILE @i<=@idUltimo  
   BEGIN  
       ---OBTENER LOS VALORES DE LAS CATEGOR�AS POR DEFECTO
       SET @existe=0;
       SET @nombreP='';
       SET @predefinida=0;
       SELECT @existe=1 FROM Predeterminada WHERE idPredeterminada=@i;
       IF @existe=1 BEGIN
         SELECT @nombreP=nombre,@predefinida=predeterminada FROM Predeterminada WHERE idPredeterminada=@i;
         ---ASOCIA UNA A UNA CADA CATEGOR�A POR DEFECTO AL USUARIO REGISTRADO
         INSERT INTO Categoria(nombre,predefinida,idUsuario)VALUES(@nombreP,@predefinida,@idUsuario);
       END
       PRINT 'LA CATEGOR�A PREDETERMINADA '+CONVERT(VARCHAR,@nombreP)+' HA SIDO A�ADIDA AL USUARIO CON EL ID '+CONVERT(VARCHAR,@idUsuario);
       SET @i=@i+1;
   END 
   ---INSERTAMOS LAS CARTERAS POR DEFECTO
   INSERT INTO Cartera(nombre,saldo,creditoMaximo,idUsuario,idTipoCartera)VALUES('Tarjeta de cr�dito',0.00,0.00,@idUsuario,1);
   INSERT INTO Cartera(nombre,saldo,idUsuario,idTipoCartera)VALUES('Otro',0.00,@idUsuario,2);
END;
GO
CREATE PROCEDURE pa_modificarUsuario
---ESTE PROCEDIMIENTO ALMACENADO MODIFICA LOS DATOS DE UN USUARIO
@idUsuario INT,
@nombre VARCHAR(45),
@apellidoP VARCHAR(45),
@apellidoM VARCHAR(45),
@correo VARCHAR(150),
@contrasena VARCHAR(22)
AS
BEGIN
   UPDATE Usuario SET nombre=@nombre,apellidoP=@apellidoP,apellidoM=@apellidoM,correo=@correo,
   contrasena=@contrasena WHERE idUsuario=@idUsuario;
END;
GO


CREATE PROCEDURE pa_imagenPerfilUsuario
--PROCEDIMIENTO ALMACENADO QUE RETORNA EL ID Y EL NOMBRE DE LA FOTO DE PERFIL DEL USUARIO
@idUsuario int
AS
BEGIN
 SELECT Imagen.idImagen,Imagen.nombre from Imagen inner join 
 Usuario on Usuario.fotoPerfil = Imagen.idImagen and Usuario.idUsuario=@idUsuario;
END;
GO

CREATE PROCEDURE pa_CambiarImagenPerfilUsuario
@imagenPerfil int,
@idUsuario int
AS
BEGIN
 UPDATE Usuario set fotoPerfil=@imagenPerfil where idUsuario=@idUsuario;
 select * from usuario
END;
GO

CREATE PROCEDURE pa_visualizarUsuario
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LOS DATOS DE UN USUARIO
@idUsuario INT
AS
BEGIN
   SELECT nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil FROM Usuario WHERE idUsuario=@idUsuario;
END;
GO

CREATE PROCEDURE pa_eliminarUsuario
---ESTE PROCEDIMIENTO ALMACENADO ELIMINA LA CUENTA DE UN USUARIO EN ESPECIFICO
@idUsuario INT
AS
BEGIN
    DELETE Gasto WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO REFERENCIAS CON LA TABLA GASTO';
    DELETE Ingreso WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO REFERENCIAS CON LA TABLA INGRESO';
    DELETE Transferencia WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO REFERENCIAS CON LA TABLA TRANSFERENCIA';
    DELETE Categoria WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO REFERENCIAS CON LA TABLA CATEGORIA';   
    DELETE Cartera WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO REFERENCIAS CON LA TABLA CARTERA';  
    DELETE Usuario WHERE idUsuario=@idUsuario;
    PRINT'SE HAN ELIMINADO LA TABLA USUARIO';  
END;
GO






---INICIA CREACCI�N DE PROCEDIMEINTOS ALMACENADOS PARA CARTERA

CREATE PROCEDURE pa_registrarCartera
---ESTE PROCEDIMIENTO ALMACENADO INSERTA UNA NUEVA CARTERA A UN USUARIO, DEVUELVE 1 � 0
---DEVUELVE 1 CUANDO SE HA AGREGADO LA CARTERA CORRECTAMENTE
---DEVUELVE 0 CUANDO NO SE HA AGREGADO DEBIDO A QUE YA EXISTE EN LA BASE DE DATOS
@nombre VARCHAR(45),
@saldo MONEY,
@creditoMaximo MONEY,
@idUsuario INT,
@idTipoCartera INT,
@insertado BIT OUTPUT 
AS
BEGIN
   DECLARE @existe BIT;
   SET @existe=0;
   SELECT @existe=1 FROM Cartera WHERE idUsuario=@idUsuario AND nombre=@nombre;
   IF @existe=0 BEGIN
       INSERT INTO Cartera(nombre,saldo,creditoMaximo,idUsuario,idTipoCartera)VALUES(@nombre,@saldo,@creditoMaximo,@idUsuario,@idTipoCartera);
       PRINT 'SE HA REGISTRADO LA CARTERA CORRECTAMENTE';
       SET @insertado=1;
   END
   ELSE BEGIN
      PRINT 'NO SE HA PODIDO REGISTRAR LA CARTERA DEBIDO A QUE YA EXISTE EN LA BASE DE DATOS';
      SET @insertado=0;
   END
END;
GO
--drop procedure pa_modificarCartera
CREATE PROCEDURE pa_modificarCartera
---ESTE PROCEDIMIENTO PERMITE MODIFICAR LOS DATOS DE UNA CARTERA
@nombre VARCHAR(45),
@saldo MONEY,
@creditoMaximo MONEY,
@idTipoCartera INT,
@idCartera INT
AS
BEGIN
   UPDATE Cartera SET nombre=@nombre,saldo=@saldo,creditoMaximo=@creditoMaximo,idTipoCartera=@idTipoCartera WHERE idCartera=@idCartera;
END;
GO
--drop procedure pa_eliminarCartera
CREATE PROCEDURE pa_eliminarCartera
---ESTE PROCEDIMIENTO ALMACENADO ELIMINA UNA CARTERA, DEVUELVE 1 � 0 
--- 1 CUANDO LA CARTERA HA SIDO ELIMINADA
--- 0 CUANDO NO SE HA PODIDO ELIMINAR DEBIDO A QUE OTROS REGISTROS DEPENDENDEN DE ESA CARTERA
@idCartera INT,
@eliminado BIT OUTPUT
AS
BEGIN
 DECLARE @existeGasto BIT;
 DECLARE @existeIngreso BIT;
 DECLARE @existeTransferencia BIT;
 SET @existeGasto=0;
 SET @existeIngreso=0;
 SET @existeTransferencia=0;
 SELECT @existeGasto=1 FROM Cartera AS C JOIN Gasto AS G ON C.idCartera=G.idCartera WHERE C.idCartera=@idCartera;
 SELECT @existeIngreso=1 FROM Cartera AS C JOIN Ingreso AS I ON C.idCartera=I.idCartera WHERE C.idCartera=@idCartera;
 SELECT @existeTransferencia=1 FROM Cartera AS C JOIN Transferencia AS T ON C.idCartera=T.idCartera WHERE C.idCartera=@idCartera;
 IF @existeGasto=0 AND @existeIngreso=0 AND @existeTransferencia=0 BEGIN
    DELETE Cartera WHERE idCartera=@idCartera;
    PRINT 'SE HA ELIMINADO LA CARTERA';
    SET @eliminado=1;
 END
 ELSE BEGIN
    PRINT 'NO SE PUEDE ELIMINAR LA CARTERA';
    IF @existeGasto=1 BEGIN
       PRINT 'ELIMINANDO REFERENCIAS DE LA TABLA GASTO';
	   DELETE FROM Gasto WHERE idCartera=@idCartera;
    END
    IF @existeIngreso=1 BEGIN
	   DELETE FROM Ingreso WHERE idCartera=@idCartera;
       PRINT 'ELIMINANDO REFERENCIAS A LA TABLA INGRESO';
    END
    IF @existeTransferencia=1 BEGIN	
	   --DELETE FROM Transferencia WHERE idCartera=@idCartera OR idCarteraQuita=@idCartera;
    --   PRINT 'ELIMINANDO REFERENCIAS EN LA TABLA TRANSFERENCIA';

	   ALTER TABLE Transferencia NOCHECK CONSTRAINT fk_transferencia_carteraQuita;
	   UPDATE Transferencia SET idCarteraQuita=NULL WHERE idCartera=@idCartera;
	   DELETE Transferencia WHERE idCartera=@idCartera;
	   DELETE FROM Cartera WHERE idCartera=@idCartera;
	   PRINT 'SE HA ELIMINADO LA CARTERA';
	   SET @eliminado=1;
	   ALTER TABLE Transferencia CHECK CONSTRAINT fk_transferencia_carteraQuita;   
    END
 END
END;
GO

CREATE PROCEDURE pa_mostrarCartera
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LOS DATOS DE UNA CATEGORIA EN ESPECIFICO
@idCartera INT
AS
BEGIN
  SELECT idCartera,nombre,saldo,creditoMaximo,idUsuario,idTipoCartera FROM Cartera WHERE idCartera=@idCartera;
END;
GO

CREATE PROCEDURE pa_mostrarCarterasCredito
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LAS CARTERAS DE CR�DITO QUE TIENE REGISTRADAS UN USUARIO  
@idUsuario INT
AS
BEGIN
  SELECT idCartera,nombre,saldo,creditoMaximo,idUsuario,idTipoCartera FROM Cartera WHERE idUsuario=@idUsuario AND idTipoCartera=1;
END;

GO

CREATE PROCEDURE pa_mostrarCarterasOtro
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LAS CARTERAS DE OTRO TIPO QUE TIENE REGISTRADAS UN USUARIO  
@idUsuario INT
AS
BEGIN
  SELECT idCartera,nombre,saldo,creditoMaximo,idUsuario,idTipoCartera FROM Cartera WHERE idUsuario=@idUsuario AND idTipoCartera=2;
END;
GO


CREATE PROCEDURE pa_mostrarCarteras
@idUsuario INT
AS
BEGIN
 SELECT idCartera,nombre,saldo,creditoMaximo,idUsuario,idTipoCartera,saldo FROM Cartera WHERE idUsuario=@idUsuario;
END;
GO


CREATE PROCEDURE pa_mostrarTipoCartera
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LOS TIPOS DE CARTERA QUE EXISTEN EN LA BASE DE DATOS
AS
BEGIN
 SELECT * FROM tipoCartera;
END;
GO

---INICIA CREACCI�N DE PROCEDIMEINTOS ALMACENADOS PARA CATEGOR�A

CREATE PROCEDURE pa_insertarCategoria
---ESTE PROCEDIMIENTO ALMACENADO INSERTA UNA CATEGORIA A UN USUARIO, DEVUELVE 1 � 0 
---DEVUELVE 1 CUANDO SE HA AGREGADO LA CATEGOR�A A LA CUENTA DEL USUARIO
---DEVULEVE 0 CUANDO EL USUARIO YA TIENE UNA CARTERA REGISTRADA CON ESE NOMBRE
@nombre VARCHAR(45),
@idUsuario INT,
@insertado BIT OUTPUT
AS
BEGIN
  DECLARE @existe BIT;
  SET @existe=0;
  SELECT @existe=1 FROM Categoria WHERE idUsuario=@idUsuario AND nombre=@nombre;
  IF @existe=0 BEGIN
     INSERT INTO Categoria(nombre,idUsuario)VALUES(@nombre,@idUsuario);
     SET @insertado=1;
  END;
  ELSE BEGIN
     PRINT 'NO SE HA PODIDO AGREGAR LA CATEGOR�A PORQUE YA EXISTE';
     SET @insertado=0;
  END 
END;
GO	

CREATE PROCEDURE pa_mostrarCategoria
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA LOS DATOS DE UNA CATEGOR�A EN ESPECIFICO
@idCategoria INT
AS
BEGIN
  SELECT idCategoria,nombre,predefinida,idUsuario FROM Categoria WHERE idCategoria=@idCategoria;
END;
GO

CREATE PROCEDURE pa_mostrarCategorias
---ESTE PROCEDIMIENTO ALMACENADO MUESTRA TODAS LAS CATEGOR�AS QUE TIENE REGISTRADAS UN USUARIO
@idUsuario INT
AS
BEGIN
  SELECT idCategoria,nombre,predefinida,idUsuario FROM Categoria WHERE idUsuario=@idUsuario;
END;
GO

CREATE PROCEDURE pa_modificarCategoria
---ESTE PROCEDIMIENTO ALMACENADO MODIFICA LOS DATOS DE UNA CATEGORIA
@idCategoria INT,
@nombre VARCHAR(45)
AS
BEGIN
  UPDATE Categoria SET nombre=@nombre WHERE idCategoria=@idCategoria;
END;
GO

CREATE PROCEDURE pa_eliminarCategoria
---ESTE PROCEDIMIENTO ALMACENADO ELIMINA UNA CATEGOR�A, DEVULEVE 1 � 0
---DEVUELVE 1 CUANDO LA CATEGOR�A HA SIDO ELIMINADA CORRECTAMENTE
--*DEVUELVE 0 CUANDO NO SE HA PODIDO ELIMINAR, YA QUE TIENE REFERENCIAS CON LA TABLA GASTO
@idCategoria INT,
@eliminado BIT OUTPUT
AS
BEGIN
  DECLARE @existeGasto BIT;
  SET @existeGasto=0;
  SELECT @existeGasto=1 FROM Categoria AS C JOIN Gasto AS G ON C.idCategoria=G.idCategoria WHERE C.idCategoria=@idCategoria;
  IF @existeGasto=0 BEGIN
     DELETE Categoria WHERE idCategoria=@idCategoria;
     PRINT 'SE HA ELIMINADO LA CATEGOR�A CORRECTAMENTE';
     SET @eliminado=1;
  END
  ELSE BEGIN
     PRINT 'NO SE HA PODIDO ELIMINAR LA CATEGORIA, DEBIDO A QUE TIENE REFERENCIAS CON LA TABLA GASTO';
     SET @eliminado=0;
  END
END;
GO





---INICIA CREACCI�N DE PROCEDIMEINTOS ALMACENADOS PARA TRANSFERENCIA

CREATE PROCEDURE pa_insertarTransferencia
---PROCEDIMIENTO ALMACENADO QUE REALIZA UNA TRANSFERENCIA DE DINERO DE UNA CARTERA A OTRA,DEVUELVE 1 � 0
---DEVUELVE 1 SI LA TRANSACCI�N FUE HECHA CON �XITO
---DEVUELVE 0 CUANDO NO SE HA PODIDO REALIZAR LA TRANSACCI�N DEBIDO A FONDOS INSUFICIENTES
@monto MONEY,
@fecha DATE,
@idCarteraQuita INT,
@idCartera INT,
@idUsuario INT,
@registrado BIT OUTPUT
AS
BEGIN
   DECLARE @saldoQuita MONEY;
   DECLARE @saldoCartera MONEY;
   DECLARE @nombreQuita VARCHAR(45);
   DECLARE @nombreAgregar VARCHAR(45);
   SELECT @saldoQuita=saldo,@nombreQuita=nombre FROM Cartera WHERE idCartera=@idCarteraQuita;
   SELECT @saldoCartera=saldo,@nombreAgregar=nombre FROM Cartera WHERE idCartera=@idCartera;
   IF @monto <= @saldoQuita BEGIN
       PRINT 'SE PROCEDE A REALIZAR LA TRANSFERENCIA';
       PRINT 'DESCRIPCI�N DE LA TRANSACCI�N';
       PRINT 'SE RETIRO DE CARTERA: '+CONVERT(VARCHAR,@nombreQuita)+'    SALDO ORIGINAL: $'+CONVERT(VARCHAR,@saldoQuita);
       PRINT 'SE AGREGO A CARTERA: '+CONVERT(VARCHAR,@nombreAgregar)+'   SALDO ORIGINAL: $'+CONVERT(VARCHAR,@saldoCartera);
       SET @saldoQuita=@saldoQuita-@monto;
       SET @saldoCartera=@saldoCartera+@monto;
       INSERT INTO Transferencia(monto,fecha,idCarteraQuita,idCartera,idUsuario)VALUES(@monto,@fecha,@idCarteraQuita,@idCartera,@idUsuario);
       UPDATE Cartera SET saldo=@saldoQuita WHERE idCartera=@idCarteraQuita;
       UPDATE Cartera SET saldo=@saldoCartera WHERE idCartera=@idCartera;
       PRINT 'AHORA...';
       PRINT 'CARTERA: '+CONVERT(VARCHAR,@nombreQuita)+'     NUEVO SALDO: $'+CONVERT(VARCHAR,@saldoQuita);
       PRINT 'CARTERA: '+CONVERT(VARCHAR,@nombreAgregar)+'   NUEVO SALDO: $'+CONVERT(VARCHAR,@saldoCartera);
       SET @registrado=1;
   END  
   ELSE BEGIN
       PRINT 'NO SE PUEDE REALIZAR LA TRANSACCI�N, SALDO INSIFICIENTE';
       SET @registrado=0;
   END
END;
GO

CREATE PROCEDURE pa_mostrarTransferencias
---PROCEDIMIENTO ALMACENADO QUE MUESTRA TODAS LAS TRANSFERENCIAS QUE REALIZ� UN USUARIO
@idUsuario INT
AS
BEGIN
   SELECT T.idTransferencia,T.monto,CONVERT(VARCHAR(10),T.fecha,103) AS fecha,T.idCarteraQuita,CQ.nombre
   AS nombreQuita,CP.idCartera,CP.nombre AS nombrePon,T.idUsuario
   FROM Transferencia AS T INNER JOIN
   Cartera AS CQ ON T.idCarteraQuita=CQ.idCartera INNER JOIN Cartera AS CP
   ON T.idCartera=CP.idCartera WHERE
   DATEPART ( month , fecha) = DATEPART ( month , getdate()) and T.idUsuario =@idUsuario;
END;
GO
--DROP PROCEDURE pa_mostrarTransferencias;
select * from Usuario
GO
CREATE PROCEDURE pa_mostrarTransferencia
---MUESTRA LOS DATOS DE UNA TRANSFERENCIA EN ESPECIFICO
@idTransferencia INT
AS
BEGIN
   SELECT idTransferencia,monto,fecha,idCarteraQuita,idCartera,idUsuario FROM Transferencia WHERE idTransferencia=@idTransferencia;
END;
GO







---INICIA CREACCI�N DE PROCEDIMEINTOS ALMACENADOS PARA INGRESO

CREATE PROCEDURE pa_insertarIngreso
---PROCEDIMEINTO ALMACENADO QUE REALIZA UN INGRESO A UNA CARTERA, DEVUELVE 1 � 0
---DEVUELVE 1 CUANDO SE HA REALIZADO CON �XITO EL INGRESO
---DEVUELVE 0 CUANDO NO SE HA PODIDO REALIZAR EL INGRESO DEBIDO A QUE EL MONTO A AGREGAR NO ERA MAYOR DE $0 PESOS
@descripcion VARCHAR(45),
@monto MONEY,
@fecha DATE,
@idCartera INT,
@idUsuario INT,
@registrado BIT OUTPUT
AS
BEGIN
     DECLARE @saldoOriginal MONEY;
     DECLARE @nuevoSaldo MONEY;
     SELECT @saldoOriginal=saldo FROM Cartera WHERE idCartera=@idCartera;
     IF @monto>0 BEGIN
        PRINT 'SE PROCEDE A REALIZAR EL INGRESO A LA CARTERA';
        SET @nuevoSaldo=@saldoOriginal+@monto;
        UPDATE Cartera SET saldo=@nuevoSaldo WHERE idCartera=@idCartera;
        INSERT INTO Ingreso(descripcion,monto,fecha,idCartera,idUsuario)VALUES(@descripcion,@monto,@fecha,@idCartera,@idUsuario);
        PRINT 'DETALLES DEL INGRESO:'
        PRINT 'SALDO ANTERIOR DE LA CARTERA: $'+CONVERT(VARCHAR,@saldoOriginal);
        PRINT 'SE LE AGREGO: $'+CONVERT(VARCHAR,@monto);
        PRINT 'NUEVO SALDO DE LA CARTERA: $'+CONVERT(VARCHAR,@nuevoSaldo);
        SET @registrado=1;
     END
     ELSE BEGIN
        PRINT 'NO SE PUEDE REALIZAR UNA TRANSFERENCIA MENOR A 1 PESO';
        SET @registrado=0;
     END
END;
GO

CREATE PROCEDURE pa_mostrarIngreso
---PROCEDIMIENTO ALMACENADO QUE MUESTRA LA INFORMACI�N DE UN INGRESO EN ESPECIFICO
@idIngreso INT
AS
BEGIN
  SELECT idIngreso,descripcion,monto,fecha,idCartera,idUsuario FROM Ingreso WHERE idIngreso=@idIngreso;
END;
GO

CREATE PROCEDURE pa_mostrarIngresos
---PROCEDIMIENTO ALMACENADO QUE MUESTRA TODOS LOS INGRESOS QUE HA REALIZADO UN CLIENTE
@idUsuario INT
AS
BEGIN
  SELECT idIngreso,descripcion,monto,fecha,idCartera,idUsuario FROM Ingreso WHERE idUsuario=@idUsuario;
END;
GO






---INICIA CREACCI�N DE PROCEDIMEINTOS ALMACENADOS PARA GASTO

--CREATE PROCEDURE pa_actualizarBorrado
--@idUsuario INT
--AS
--BEGIN
--   UPDATE Gasto SET borrar=1 WHERE idGasto=@idUsuario AND fecha<DATEADD(DAY,-30,GETDATE()) ; 
--END;
--GO

CREATE PROCEDURE pa_actualizarBorrado
--ESTE PROCEDIMIENTO HABLITA LA OPCION DE ELIMINAR GASTOS CON MAYOR A 30 D�AS DE ANTIGUEDAD
@idUsuario INT
AS
BEGIN
  DECLARE @i INT;
  DECLARE @idUltimo INT;
  DECLARE @existe INT;
  DECLARE @fecha DATE;
  DECLARE @id INT;
  SELECT TOP 1 @idUltimo=idGasto FROM Gasto ORDER BY idGasto DESC;
  SET @i=1;
  WHILE @i<=@idUltimo  
   BEGIN  
       ---OBTENER LOS VALORES DE LAS CATEGOR�AS POR DEFECTO
       SET @existe=0;
       SELECT @existe=1,@fecha=fecha,@id=idUsuario FROM Gasto WHERE idGasto=@i;
       IF @existe=1 AND @fecha<DATEADD(DAY,-30,GETDATE())AND @id=@idUsuario BEGIN
         UPDATE Gasto SET borrar=1 WHERE idGasto=@i; 
       END
       ELSE BEGIN 
         UPDATE Gasto SET borrar=0 WHERE idGasto=@i;
       END    
       SET @i=@i+1;
   END 
END;
GO

CREATE PROCEDURE pa_insertarGasto
---PROCEDIMIENTO ALMACENADO QUE REALIZA UN GASTO, EL MONTO GASTADO SE LE RESTA A UNA CARTERA EN ESPECIFICO, DEVUELVE 1 � 0
---DEVULEVE 1 CUANDO SE HA REALIZADO EL GASTO CORRECTAMENTE
---DEVULEVE 0 CUANDO NO SE HA REALIZADO, DEBIDO A FALTA DE FONDOS DE LA CARTERA CON LA QUE SE INTENTA PAGAR
@descripcion VARCHAR(300),
@monto MONEY,
@fecha DATE,
@idCategoria INT,
@idCartera INT,
@idUsuario INT,
@registrado BIT OUTPUT
AS
BEGIN
   DECLARE @saldoCartera MONEY;
   DECLARE @nuevoSaldo MONEY;
   DECLARE @nombreCartera VARCHAR(45);
   SELECT @saldoCartera=saldo,@nombreCartera=nombre FROM Cartera WHERE idCartera=@idCartera;
   IF @monto<=@saldoCartera BEGIN
      PRINT 'SE PROCEDE A REALIZAR LA OPERACI�N DE GASTO';
      SET @nuevoSaldo=@saldoCartera-@monto;
      INSERT INTO Gasto(descripcion,monto,fecha,idCategoria,idCartera,idUsuario)VALUES(@descripcion,@monto,@fecha,@idCategoria,@idCartera,@idUsuario);
      UPDATE Cartera SET saldo=@nuevoSaldo WHERE idCartera=@idCartera;
      PRINT 'DESCRIPCI�N DE LA TRANSACCI�N';
      PRINT 'CARTERA: '+CONVERT(VARCHAR,@nombreCartera)+'    SALDO ANTERIOR: $'+CONVERT(VARCHAR,@saldoCartera)+'    NUEVO SALDO: $'+CONVERT(VARCHAR,@nuevoSaldo);     
      SET @registrado=1;
   END 
   ELSE BEGIN
      PRINT 'NO SE HA PODIDO REALIZAR EL GASTO, SALDO INSUFICIENTE DE LA CARTERA A RETIRAR';
      SET @registrado=0;
   END  
END;
GO

CREATE PROCEDURE pa_mostrarGasto
---PROCEDIMIENTO ALMACENADO QUE MUESTRA LA INFORMACI�N DE UN GASTO EN ESPECIFICO
@idGasto INT
AS
BEGIN
  SELECT idGasto,descripcion,monto,fecha,idCategoria,idCartera,idUsuario FROM Gasto WHERE idGasto=@idGasto;
END;
GO

CREATE PROCEDURE pa_mostrarGastos
---PROCEDIMIENTO ALMACENADO QUE MUESTRA TODOS LOS GASTOS QUE HA REALIZADO UN CLIENTE
@idUsuario INT
AS
BEGIN
  SELECT G.idGasto,G.descripcion,G.monto,CONVERT(VARCHAR(10),G.fecha,103) AS fecha,C.idCategoria AS idCat,C.nombre AS nombreCategoria,CA.idCartera AS idCar,CA.nombre AS nombreCartera,G.idUsuario,G.borrar FROM Gasto AS G INNER JOIN Categoria AS C ON G.idCategoria=C.idCategoria 
  INNER JOIN Cartera AS CA ON G.idCartera=CA.idCartera WHERE G.idUsuario=@idUsuario;
END;
GO
--DROP PROCEDURE pa_mostrarGastos
CREATE PROCEDURE pa_eliminarGasto
--PROCEDIMIENTO ALMACENADO QUE ELIMINA UN GASTO
@idGasto INT
AS
BEGIN
  DELETE Gasto WHERE idGasto=@idGasto;
END;
GO


---FINALIZA CREACI�N DE LOS PROCEDIMIENTOS ALMACENADOS-------------------------------------------------------------------------------------


---FINALIZA CREACI�N DE LOS PROCEDIMIENTOS ALMACENADOS-------------------------------------------------------------------------------------






---INICIA EL TESTEO DE ALGUNOS PROCEDIMIENTOS ALMACENADOS----------------------------------------------------------------------------------


---PROBAR PROCEDIMIENTO ALMACENADO pa_registrarTransferencia---------------------
SELECT * FROM Usuario WHERE idUsuario=4;
exec pa_mostrarIngresos 4
execute pa_eliminarUsuario 6
GO
SELECT * FROM Cartera WHERE idUsuario=7; 
GO
DECLARE @devuelve INT;
EXECUTE pa_insertarTransferencia 2500,'2016-11-17',5,6,3,@devuelve OUTPUT;
GO
SELECT * FROM Transferencia WHERE idUsuario=3;
GO
---------------------------------------------------------------------------------

---PROBAR PROCEDIMIENTO ALMACENADO pa_registrarIngreso---------------------------
SELECT * FROM Cartera WHERE idCartera=5;
GO
DECLARE @devuelveIngreso INT;
EXECUTE pa_insertarIngreso 'Por si acaso',3000,'2016-12-17',5,3,@devuelveIngreso OUTPUT;
GO
SELECT * FROM Ingreso WHERE idUsuario=3;
GO
---------------------------------------------------------------------------------

---PROBAR PROCEDIMIENTO ALMACENADO pa_insertarUsuario----------------------------
EXECUTE pa_insertarUsuario 'Jos�','Narvaez','Figueroa','jose@gmail.com','123';
GO
SELECT * FROM Usuario;
SELECT * FROM Categoria;
SELECT * FROM Cartera;
GO
---------------------------------------------------------------------------------

---PROBAR PROCEDIMIENTO ALMACENADO pa_insertarGasto------------------------------
DECLARE @devuelveGasto BIT;
EXECUTE pa_insertarGasto 'Pago de recibo de TELMEX',380,'2016-11-18',3,6,5,@devuelveGasto OUTPUT;
GO
SELECT * FROM Gasto;
GO
---------------------------------------------------------------------------------

---PROBAR PROCEDIMIENTO ALMACENADO pa_registrarCartera---------------------------
DECLARE @INSER BIT;
EXECUTE pa_registrarCartera 'Colchon',14500,null,1,2,@INSER OUTPUT;
GO
SELECT * FROM Cartera;
---------------------------------------------------------------------------------

---CONSULTAS 			
SELECT * FROM Usuario;
SELECT * FROM Icono;
SELECT * FROM tipoCartera;
SELECT * FROM Cartera;
SELECT * FROM Categoria;
SELECT * FROM Predeterminada;
SELECT * FROM Ingreso;
SELECT * FROM Transferencia;
SELECT * FROM Deuda;









































----APARTIR DE AQUI NO EJECUTAR

INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	SELECT 'Sarahi Daniela','Medina','Martinez','penlove87@hotmail.com','123456789', BulkColumn  FROM Openrowset( 
			Bulk 'C:\Users\Public\Pictures\Sample Pictures\Koala.jpg', Single_Blob) as fotoPerfil;
			
INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	SELECT 'Celica','Cabrera','S�nchez','celica@gmail.com','2223335896', BulkColumn  FROM Openrowset( 
			Bulk 'C:\Users\Public\Pictures\Sample Pictures\Koala.jpg', Single_Blob) as fotoPerfil;
			
INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	SELECT 'Francisco','Gardu�o','Estrada','francisco@gmail.com','12569863', BulkColumn  FROM Openrowset( 
			Bulk 'C:\Users\Public\Pictures\Sample Pictures\Koala.jpg', Single_Blob) as fotoPerfil;

INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	SELECT 'Ricardo Lino','Garc�a','L�pez','ricardo@gmail.com','7788999636', BulkColumn  FROM Openrowset( 
			Bulk 'C:\Users\Public\Pictures\Sample Pictures\Koala.jpg', Single_Blob) as fotoPerfil;

INSERT INTO Usuario(nombre,apellidoP,apellidoM,correo,contrasena,fotoPerfil)  
	SELECT 'Giovanni','Garc�a','Ferrari','giovanni@gmail.com','558899', BulkColumn  FROM Openrowset( 
			Bulk 'C:\Users\Public\Pictures\Sample Pictures\Koala.jpg', Single_Blob) as fotoPerfil;

			select descripcion, convert(decimal(7,2),monto,2) as Monto, convert(varchar(10),fecha,103)as fecha, Ingreso.idCartera, Cartera.nombre as cartera, Ingreso.idUsuario from Ingreso inner join Cartera on Ingreso.idCartera=Cartera.idCartera
where  DATEPART ( month , fecha) = DATEPART ( month , getdate()) and Ingreso.idUsuario=4;