/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function hideSaldo(){
    document.getElementById('saldoContenedor').style.display ="none";
}


function validarSaldoCreditoCartera(){
    var saldo = document.getElementById('saldoBox').value*1.0;
    var credMax = document.getElementById('creditBox').value*1.0;
    var listaTipos = document.getElementById('lista').value;
    if (listaTipos === '2') {
        return true;
    }else if(saldo > credMax ){
        swal('El saldo no puede ser mayor al crédito máximo','','error');
        return false;
    }
    return true;
}

function modificarCartera(idCartera) {
    var tipoCartera = document.getElementById('idTC' + idCartera).value;
    var label = document.getElementById('nuevoCreditoMaxCarteraLabel');
    var cajaCeditoMaximo = document.getElementById('nuevoCreditoMaxCartera');
    if (tipoCartera === '2') {
        cajaCeditoMaximo.style.visibility = "hidden";
        label.style.visibility = "hidden";
    } else {
        cajaCeditoMaximo.style.visibility = "visible";
        label.style.visibility = "visible";
    }
    document.getElementById('idMiCarterita').value = document.getElementById('idMiCarterita' + idCartera).value;
    document.getElementById('idTC').value = document.getElementById('idTC' + idCartera).value;
    document.getElementById('nuevoNombreCartera').value = document.getElementById('nombreCartera' + idCartera).value;
    document.getElementById('nuevoSaldoCartera').value = document.getElementById('saldoCartera' + idCartera).value;
    document.getElementById('nuevoCreditoMaxCartera').value = document.getElementById('creditoMaxCartera' + idCartera).value;
}


function crearCarteraFields() {
    var label = document.getElementById('creditLabel');
    var box = document.getElementById('creditBox');
    var listaTipos = document.getElementById('lista').value;

    if (listaTipos === '2') {
        label.style.visibility = "hidden";
        box.style.visibility = "hidden";
        box.value = 0;
    } else {
        label.style.visibility = "visible";
        box.style.visibility = "visible";
        box.value = "";
    }
}

function verifDatosPerfil(){
    var currentPass = document.getElementById('currentPass').value;
    var currentPassVerif = document.getElementById('currentPassVerif').value;
    var newPass1 = document.getElementById('newPass1').value;
    var newPass2 = document.getElementById('newPass2').value;
    
    if (currentPassVerif === '' && newPass1 === '' && newPass2 === '') {
        return true;
    }else if(currentPass === currentPassVerif){
        if (newPass1 !== '' && newPass2 !== '') {
            if (newPass1 === newPass2) {
                document.getElementById('currentPass').value = newPass1;
                return true;
            }else{
                swal('Las contraseñas no coinciden','','error');
                return false;
            }
        }else{
            swal('La nueva contraseña está en blanco','','error');
            return false;
        }
    }else if(currentPass !== currentPassVerif){
        currentPassVerif = document.getElementById('currentPassVerif').value = '';
        currentPassVerif = document.getElementById('currentPassVerif').focus();
        swal('Tu contraseña actual no coincide','','error');
        return false;
    }else if(newPass1 !== newPass2){
        swal('Las contraseñas no coinciden');
        return false;
    }
    return true;
}



