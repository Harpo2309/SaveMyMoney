/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('.btnEliminar').click(function () {
        var id = $(this).data('idCartera');

        swal({
            title: "¿Estás seguro de eliminar tu cartera?",
            text: "Se perderá toda la información relacionada",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, eliminar",
            cancelButtonText: "¡No, espera!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#idForm').val(id);
                        $('#formElim').submit();
                    } else {
                        swal("Eliminación cancelada", "Tu cartera está segura :)", "error");
                    }
                });
    });

});

$(document).ready(function () {
    $('.btnEliminarGasto').click(function () {
        var id = $(this).data('idGasto');

        swal({
            title: "Eliminar gasto",
            text: "¿Estás seguro de eliminar el gasto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, eliminar",
            cancelButtonText: "¡No, espera!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#idFormGasto').val(id);
                        $('#formElimGasto').submit();
                    } else {
                        swal("Eliminación cancelada", "No hemos eliminado nada :)", "error");
                    }
                });
    });

});

$(document).ready(function () {
    $('.btnEliminarCategoria').click(function () {
        var id = $(this).data('idCategoria');

        swal({
            title: "Eliminar categoría",
            text: "¿Estás seguro de eliminar la categoría?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, eliminar",
            cancelButtonText: "¡No, espera!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#idFormCategoria').val(id);
                        $('#formElimCategoria').submit();
                    } else {
                        swal("Eliminación cancelada", "No hemos eliminado nada :)", "error");
                    }
                });
    });

});

function validModifCartera() {
    var creditMax = document.getElementById('nuevoCreditoMaxCartera').value * 1.0;
    var saldo = document.getElementById('nuevoSaldoCartera').value * 1.0;
    var tipoCartera = document.getElementById('idTC').value;
    if (tipoCartera === '2') {
        if (creditMax === '0') {
            return true;
        }
    } else if (creditMax >= saldo) {
        return true;
    } else {
        document.getElementById('nuevoCreditoMaxCartera').focus();
        swal('Aviso', 'El saldo no puede ser mayor al credito disponible. \n\nCrédito máximo: $' + creditMax + '\nSaldo: $' + saldo, 'error');
        return false;
    }
    return true;
}

function validModifPerfil() {
    var creditMax = document.getElementById('nuevoCreditoMaxCartera').value * 1.0;
    var saldo = document.getElementById('nuevoSaldoCartera').value * 1.0;
    var tipoCartera = document.getElementById('idTC').value;
    if (tipoCartera === '2') {
        if (creditMax === '0') {
            return true;
        }
    } else if (creditMax >= saldo) {
        return true;
    } else {
        document.getElementById('nuevoCreditoMaxCartera').focus();
        swal('Aviso', 'El saldo no puede ser mayor al credito disponible. \n\nCrédito máximo: $' + creditMax + '\nSaldo: $' + saldo, 'error');
        return false;
    }
    return true;
}