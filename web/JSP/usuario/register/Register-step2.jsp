<%-- 
    Document   : Register-step2
    Created on : 10/11/2016, 12:36:46 PM
    Author     : Harpo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

        <!--HOJAS DE ESTILO -->
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/registerStyle.css"/>
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/StyleImg.css">
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/ImageSelect.css">
        <link rel="stylesheet" type="text/css" href="<%=context%>/CSS/chosen.css">


        <!-- JS DEL SELECT DE IMAGEN PARA USER -->
        <script src="<%=context%>/JS/jquery-1.11.0.min.js"></script>
        <script src="<%=context%>/JS/chosen.jquery.js"></script>
        <script src="<%=context%>/JS/ImageSelect.jquery.js"></script>



        <!--SWEET ALERT-->
        <script src="<%=context%>/sweetalert-master/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=context%>/sweetalert-master/dist/sweetalert.css">


        <!--
        JS PARA FUNCIONES
        -->
        <script type="text/javascript">

            function back() {
                document.getElementById('back').href = "<%=context%>/JSP/usuario/register/Register-step1.jsp";
            }

            function validarPassword() {
                var pass1 = document.getElementById('pass1').value;
                var pass2 = document.getElementById('pass2').value;
                if (pass1 === pass2) {
                    document.getElementById('passOficial').value = pass1;
                    return true;
                } else {
                    swal('Las contraseñas no coinciden');
                    return false;
                }
                return true;
            }

            //muestra la imagen seleccionada
            function ShowSelected()
            {
                /* Para obtener el valor */
                var imagen = document.getElementById("sel").value;
                document.getElementById("idImg").value = imagen;
                document.getElementById('mg').src = "<%=context%>/IMG/Users/Login/" + imagen + ".jpg";

            }
        </script>
        <title>SaveMyMoney :: Crear una cuenta - Paso 2</title>
    </head>
    <body class="hold-transition login-page" style="background-image: url(<%=context%>/IMG/dregister/fondoblanco.jpg); position: center; background-size: contain" onload="ShowSelected()">
        <form action="<%=context%>/crearCuenta" method="post" onsubmit="return validarPassword()">
            <div class="container container2">
                <div class="row" id="topBanner">

                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4" id="container-land">
                        <img class="img-responsive" id="banner2" style="display: block" src="<%=context%>/IMG/register/step2-land.png"/>
                    </div>
                    <div class="col-md-4 col-sm-4" id="mainCol">
                        <div class="login-box" style="background-color: white;">
                            <div class="login-box-body" style="margin-top: -30px;">
                                <div class="login-logo" style="background-color: white;">
                                    <img class="img-responsive" src="<%=context%>/IMG/logoai.png"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <p>Correo electrónico:</p>
                                    <input type="email" required="" class="form-control" name="nuevoUsuario.correo"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Contraseña:</p>
                                    <input type="password" required="" id="pass1" class="form-control" name="pass1"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <p>Confirmar contraseña:</p>
                                    <input type="password" required="" id="pass2" class="form-control" name="pass2"/>
                                    <input type="hidden" id="passOficial" class="form-control" name="nuevoUsuario.password"/>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="imageSelector">
                            <h2>Elije un avatar</h2>
                            <select  id="sel" class="my-select form-control" onchange="ShowSelected();">
                                <s:iterator value="misImagenes" >
                                    <option  value="<s:property value="idImagen"/>"  data-img-src="<%=context%>/IMG/Users/Login/<s:property value="idImagen"/>.jpg" ><s:property value="nombre"/></option>
                                </s:iterator>
                            </select>
                            <input id="idImg"  type="hidden" name="nuevoUsuario.fotoPerfil"/>
                        </article>
                        <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <script>
                                //toma la imagen seleccionada en el select de cambio de foto de perfil
                                $(".my-select").chosen({
                                    width: "50%",
                                    html_template: '<img style="border:1px solid black;padding:0px;margin-right:5px " class="{class_name}" src="{url}">'
                                });
                            </script>


                            <div id="content">
                                <br>
                                <img  id="mg" class="img-circle" style="width: 200px; height: 200px;">
                            </div>
                        </aside>
                    </div>
                </div>
                <br><br>
                <div id="botones" class="row" align="center">
                    <div class="col-md-6">
                        <a id="back" onclick="back()" class="btn btn-link">atrás</a>
                    </div>
                    <div class="col-md-6">
                        <input type="submit" value="Crear cuenta" class="btn btn-primary" style="background-color: yellowgreen;border-color: yellowgreen;"/>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
</html>
