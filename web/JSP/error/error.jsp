<%-- 
    Document   : error
    Created on : 10/11/2016, 11:14:37 PM
    Author     : Harpo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/skins/_all-skins.min.css">
        <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <title>Error de operación</title>
    </head>
    <body>
        <style>
            #image {
                width:45%;
                height: 45%;
            }
            #contentImage{
                text-align: center;
            }
        </style>
        <div class="container">
            <div class="row" id="contentImage">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br>
                    <img id="image" src="<%=context%>/IMG/400.png" alt="image" />
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4><s:property value="message"/></h4>
                    <a href="<%=context%>/miCuenta" class="btn btn-primary">Volver</a>
                </div>
            </div>
        </div>
    </body>
</html>
