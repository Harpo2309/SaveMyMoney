<%-- 
    Document   : index
    Created on : 10/11/2016, 12:23:21 PM
    Author     : Harpo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%String context = request.getContextPath();%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<%=context%>/AdminLTE-2.3.7/dist/css/skins/_all-skins.min.css">
        <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <title>Save My Money</title>

        <!--este codigo hará que si la sesión está iniciada se redireccione al menu sin iniciar
        sesion de nuevo
        -->

        <s:if test="#session.logged == 'yes'">
            <script>
                window.location.replace("<%=context%>/miCuenta");
            </script>
        </s:if>     



    </head>
    <body class="hold-transition login-page" style="background-image: url(IMG/fondo_verde2.png); position: center; background-size: contain">
        <div class="login-box">
            <div class="login-box-body" style="margin-top: -30px;">
                <div class="login-logo" style="background-color: white;">
                    <img class="img-responsive" src="IMG/logoai.png" width="252" height="110" style="align-self:center"/>
                </div>
                <p class="login-box-msg"><strong>Iniciar sesión</strong></p>
                <p class="login-box-msg"><strong><s:property value="mensaje"/></strong></p>
                <form name="login" action="loginUsuario" method="post">
                    <div class="form-group has-feedback">
                        <p>Correo electrónico:</p>
                        <input required="" type="email" class="form-control" name="correo"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <p>Contraseña:</p>
                        <input required="" type="password" name="password" class="form-control"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="submit" value="Iniciar sesión" class="btn btn-primary" style="background-color: yellowgreen;border-color: yellowgreen;"/>
                        </div></div>
                </form>
                <br>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a href="<%=context%>/JSP/usuario/register/Register-step1.jsp">Crear cuenta</a>
                    </div>
                    
                </div>         
            </div>
        </div>
        <div class="panel-footer" style="background:#FFC414">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="copyright" style="color:white">
                            <p>© 2016, CodeLab. Todos los derechos reservados. </p>
                            <p></p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
