/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanImagenPerfil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.ConnectionBD;

/**
 *
 * @author Celeste
 */
public class DaoImagenPerfil {

    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    boolean status;

    public List<BeanImagenPerfil> listaImagen() {
        List<BeanImagenPerfil> listaC = new ArrayList();
        try {
            con = ConnectionBD.getConnection();
            pst = con.prepareStatement(" select * from Imagen;");
            rs = pst.executeQuery();
            while (rs.next()) {
                BeanImagenPerfil img = new BeanImagenPerfil();
                img.setIdImagen(rs.getInt("idImagen"));
                img.setNombre(rs.getString("nombre"));
                listaC.add(img);
            }
            System.out.println("cantidad de imagenes: " + listaC.size());
        } catch (SQLException ex) {
            Logger.getLogger(DaoImagenPerfil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error de cierre: " + e);
            }
        }
        return listaC;
    }

    public int imagenPerfil(int idUser) {
        int img = 0;
        final String qwery = "EXECUTE pa_imagenPerfilUsuario ?";
        try {
            con = ConnectionBD.getConnection();
            pst = con.prepareStatement(qwery);
            pst.setInt(1, idUser);
            rs = pst.executeQuery();
            if (rs.next()) {
                img = rs.getInt("idImagen");
            }
        } catch (SQLException e) {
            System.err.println("Error al obtener id de imagen: " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error de cierre: " + e);
            }
        }
        return img;
    }
}
