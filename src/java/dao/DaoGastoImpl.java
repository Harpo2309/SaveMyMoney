/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanCartera;
import bean.BeanCategoria;
import bean.BeanGasto;
import bean.BeanUsuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import util.ConnectionBD;
import util.interfaces.DaoGasto;

/**
 *
 * @author redes1
 */
public class DaoGastoImpl implements DaoGasto {

    Connection conexion;
    PreparedStatement pstm;
    ResultSet rs;
    CallableStatement call;

    @Override
    public boolean registrarGasto(BeanGasto gasto, int idUsuario) {
        boolean registrado = false;
        try {
            conexion = ConnectionBD.getConnection();
            call = conexion.prepareCall("{ call pa_insertarGasto (?,?,?,?,?,?,?) }");
            call.setString(1, gasto.getDescripcion());
            call.setFloat(2, gasto.getMonto());
            call.setString(3, gasto.getFecha());
            call.setInt(4, gasto.getCategoria().getIdCategoria());
            call.setInt(5, gasto.getCartera().getIdCartera());
            call.setInt(6, idUsuario);
            call.registerOutParameter(7, java.sql.Types.BIT);
            registrado = call.executeUpdate() == 1;
//            int respuesta = call.getInt(7);
//            System.out.println("respuesta: " + respuesta);
//            if (respuesta == 1) {
//                registrado = true;
//            }
        } catch (SQLException ex) {
            System.err.println("Excepción SQL: " + ex.getMessage() + "es aki");
        } catch (Exception e) {
            System.err.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception exc) {
                System.err.println("Excepción: " + exc.getMessage());
            }
        }
        return registrado;
    }

    @Override
    public List<BeanCategoria> listarCategorias(int idUsuario) {
        List<BeanCategoria> listaCategorias = new ArrayList<>();
        Statement st = null;
        try {
            conexion = ConnectionBD.getConnection();
            pstm = conexion.prepareStatement("EXECUTE pa_mostrarCategorias ?");
            pstm.setInt(1, idUsuario);
            rs = pstm.executeQuery();
            while (rs.next()) {
                BeanCategoria beanCategoria = new BeanCategoria();
                beanCategoria.setIdCategoria(rs.getInt("idCategoria"));
                beanCategoria.setNombre(rs.getString("nombre"));
                beanCategoria.setPredefinida(rs.getInt("predefinida"));
                BeanUsuario beanUsuario = new BeanUsuario();
                beanUsuario.setIdUsuario(rs.getInt("idUsuario"));
                beanCategoria.setUsuario(beanUsuario);
                listaCategorias.add(beanCategoria);
            }
            System.out.println("tamaño categorías para gasto: " + listaCategorias.size());
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception exc) {
                System.out.println("Excepción: " + exc.getMessage());
            }
        }
        return listaCategorias;
    }

    @Override
    public List<BeanCartera> listarCarteras(int idUsuario) {
        List<BeanCartera> listaCarteras = new ArrayList<>();
        Statement st = null;
        try {
            conexion = ConnectionBD.getConnection();
            Statement statement = conexion.createStatement();
            pstm = conexion.prepareStatement("EXECUTE pa_mostrarCarteras ?");
            pstm.setInt(1, idUsuario);
            rs = pstm.executeQuery();
            while (rs.next()) {
                BeanCartera beanCartera = new BeanCartera();
                beanCartera.setIdCartera(rs.getInt("idCartera"));
                beanCartera.setNombre(rs.getString("nombre"));
                beanCartera.setSaldo(rs.getFloat("saldo"));
                beanCartera.setCreditoMaximo(rs.getFloat("creditoMaximo"));
                beanCartera.setIdUsuario(rs.getInt("idUsuario"));
                beanCartera.setIdTipoCartera(rs.getInt("idTipoCartera"));
                beanCartera.setSaldo(rs.getInt("saldo"));
                listaCarteras.add(beanCartera);
            }
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception exc) {
                System.out.println("Excepción: " + exc.getMessage());
            }
        }
        return listaCarteras;
    }

    @Override
    public List<BeanGasto> listarGastos(int idUsuario) {
        List<BeanGasto> listaGastos = new ArrayList<>();
        Statement st = null;
        try {
            conexion = ConnectionBD.getConnection();
            Statement statement = conexion.createStatement();
            pstm = conexion.prepareStatement("EXECUTE pa_actualizarBorrado ?");
            pstm.setInt(1, idUsuario);
            pstm.execute();
            conexion.close();

            conexion = ConnectionBD.getConnection();
            statement = conexion.createStatement();
            pstm = conexion.prepareStatement("EXECUTE pa_mostrarGastos ?");
            pstm.setInt(1, idUsuario);
            rs = pstm.executeQuery();
            while (rs.next()) {
                BeanGasto beanGasto = new BeanGasto();
                beanGasto.setIdGasto(rs.getInt("idGasto"));
                beanGasto.setDescripcion(rs.getString("descripcion"));
                beanGasto.setMonto(rs.getFloat("monto"));
                beanGasto.setFecha(rs.getString("fecha"));
                BeanCategoria beanCategoria = new BeanCategoria();
                beanCategoria.setIdCategoria(rs.getInt("idCat"));
                beanCategoria.setNombre(rs.getString("nombreCategoria"));
                beanGasto.setCategoria(beanCategoria);
                BeanCartera beanCartera = new BeanCartera();
                beanCartera.setIdCartera(rs.getInt("idCar"));
                beanCartera.setNombre(rs.getString("nombreCartera"));
                beanGasto.setCartera(beanCartera);
                beanGasto.setBorrar(rs.getInt("borrar"));
                listaGastos.add(beanGasto);
            }
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception exc) {
                System.out.println("Excepción: " + exc.getMessage());
            }
        }
        return listaGastos;
    }

    @Override
    public boolean eliminarGasto(int idGasto) {
        boolean eliminado = false;
        try {
            conexion = ConnectionBD.getConnection();
            call = conexion.prepareCall("{ call pa_eliminarGasto (?) }");
            call.setInt(1, idGasto);
            eliminado = call.executeUpdate() == 1;
        } catch (SQLException ex) {
            System.err.println("Excepción SQL: " + ex.getMessage() + "es aki");
        } catch (Exception e) {
            System.err.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception exc) {
                System.err.println("Excepción: " + exc.getMessage());
            }
        }
        return eliminado;
    }
}
