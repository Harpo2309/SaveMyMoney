/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanCategoria;
import bean.BeanUsuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import util.ConnectionBD;
import util.interfaces.DaoCategoria;

/**
 *
 * @author Alumno
 */
public class DaoCategoriaImpl implements DaoCategoria {

    Connection conexion;
    PreparedStatement pstm;
    ResultSet rs;
    CallableStatement call;

    @Override
    public List<BeanCategoria> listarCategorias(int idUsuario) {
        List<BeanCategoria> listaCategorias = new ArrayList<>();
        Statement st = null;
        try {
            conexion = ConnectionBD.getConnection();
            Statement statement = conexion.createStatement();
            pstm = conexion.prepareStatement("SELECT * FROM Categoria WHERE idUsuario=? AND predefinida=0");
            pstm.setInt(1, idUsuario);
            rs = pstm.executeQuery();
            while (rs.next()) {
                BeanCategoria beanCategoria = new BeanCategoria();
                beanCategoria.setIdCategoria(rs.getInt("idCategoria"));
                beanCategoria.setNombre(rs.getString("nombre"));
                beanCategoria.setPredefinida(rs.getInt("predefinida"));
                BeanUsuario beanUsuario = new BeanUsuario();
                beanUsuario.setIdUsuario(rs.getInt("idUsuario"));
                beanCategoria.setUsuario(beanUsuario);
                listaCategorias.add(beanCategoria);
            }
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception exc) {
                System.out.println("Excepción: " + exc.getMessage());
            }
        }
        return listaCategorias;
    }

    @Override
    public boolean registrarCategoria(BeanCategoria categoria, int idUsuario) {
        boolean registrado = false;
        try {
            conexion = ConnectionBD.getConnection();
            call = conexion.prepareCall("{ call pa_insertarCategoria (?,?,?) }");
            call.setString(1, categoria.getNombre());
            call.setInt(2, idUsuario);
            call.registerOutParameter(3, java.sql.Types.INTEGER);
            registrado = call.executeUpdate() == 1;
        } catch (SQLException ex) {
            System.err.println("Excepción SQL: " + ex.getMessage() + "es aki");
        } catch (Exception e) {
            System.err.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception exc) {
                System.err.println("Excepción: " + exc.getMessage());
            }
        }
        return registrado;
    }

    @Override
    public List<BeanCategoria> listarCategoriasDefecto(int idUsuario) {
        List<BeanCategoria> listaCategorias = new ArrayList<>();
        Statement st = null;
        try {
            conexion = ConnectionBD.getConnection();
            Statement statement = conexion.createStatement();
            pstm = conexion.prepareStatement("SELECT * FROM Categoria WHERE idUsuario=? AND predefinida=1");
            pstm.setInt(1, idUsuario);
            rs = pstm.executeQuery();
            while (rs.next()) {
                BeanCategoria beanCategoria = new BeanCategoria();
                beanCategoria.setIdCategoria(rs.getInt("idCategoria"));
                beanCategoria.setNombre(rs.getString("nombre"));
                beanCategoria.setPredefinida(rs.getInt("predefinida"));
                BeanUsuario beanUsuario = new BeanUsuario();
                beanUsuario.setIdUsuario(rs.getInt("idUsuario"));
                beanCategoria.setUsuario(beanUsuario);
                listaCategorias.add(beanCategoria);
            }
        } catch (SQLException ex) {
            System.out.println("Excepción SQL: " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (Exception exc) {
                System.out.println("Excepción: " + exc.getMessage());
            }
        }
        return listaCategorias;
    }

    @Override
    public boolean eliminarCategoria(int idCategoria) {
        boolean eliminado = false;
        try {
            conexion = ConnectionBD.getConnection();
            call = conexion.prepareCall("{ call pa_eliminarCategoria (?,?) }");
            call.setInt(1, idCategoria);
            call.registerOutParameter(2, java.sql.Types.INTEGER);
            eliminado = call.executeUpdate() == 1;
        } catch (SQLException ex) {
            System.err.println("Excepción SQL: " + ex.getMessage() + "es aki");
        } catch (Exception e) {
            System.err.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception exc) {
                System.err.println("Excepción: " + exc.getMessage());
            }
        }
        return eliminado;
    }

    @Override
    public boolean actualizarCategoria(BeanCategoria categoria) {
        boolean actualizado = false;
        try {
            conexion = ConnectionBD.getConnection();
            call = conexion.prepareCall("{ call pa_modificarCategoria (?,?) }");
            call.setInt(1, categoria.getIdCategoria());
            call.setString(2, categoria.getNombre());
            actualizado = call.executeUpdate() == 1;
        } catch (SQLException ex) {
            System.err.println("Excepción SQL: " + ex.getMessage() + "es aki");
        } catch (Exception e) {
            System.err.println("Excepción: " + e.getMessage());
        } finally {
            try {
                if (conexion != null) {
                    conexion.close();
                }
                if (pstm != null) {
                    pstm.close();
                }
            } catch (Exception exc) {
                System.err.println("Excepción: " + exc.getMessage());
            }
        }
        return actualizado;
    }

}
