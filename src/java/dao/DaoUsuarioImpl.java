/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanUsuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import util.interfaces.DaoUsuario;
import util.ConnectionBD;

/**
 *
 * @author Harpo
 */
public class DaoUsuarioImpl implements DaoUsuario {

    private Connection con;
    private ResultSet rs;
    private PreparedStatement ps;
    private CallableStatement cs;
    private boolean status;

    @Override
    public boolean add(BeanUsuario usuario) {
        status = false;
        final String qwery = "execute pa_insertarUsuario ?,?,?,?,?,?";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareCall(qwery);
            ps.setString(1, usuario.getNombre());
            ps.setString(2, usuario.getApellidoPaterno());
            ps.setString(3, usuario.getApellidoMaterno());
            ps.setString(4, usuario.getCorreo());
            ps.setString(5, usuario.getPassword());
            ps.setInt(6, usuario.getFotoPerfil());
            status = ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Error al insertar: " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return status;
    }

    @Override
    public boolean delete(BeanUsuario usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(BeanUsuario usuario) {
        final String query = "pa_modificarUsuario ?,?,?,?,?,?";
        status = false;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, usuario.getIdUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getApellidoPaterno());
            ps.setString(4, usuario.getApellidoMaterno());
            ps.setString(5, usuario.getCorreo());
            ps.setString(6, usuario.getPassword());
            status = ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al modificar perfil: " + e);
        }
        return status;
    }

    @Override
    public BeanUsuario buscarUsuario(String correo, String pass) {
        final String qwery = "select * from Usuario where correo =? and contrasena=?";
        BeanUsuario user = null;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setString(1, correo);
            ps.setString(2, pass);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = new BeanUsuario(
                        rs.getInt("idUsuario"),
                        rs.getString("nombre"),
                        rs.getString("apellidoP"),
                        rs.getString("apellidoM"),
                        rs.getString("correo"),
                        rs.getString("contrasena"),
                        rs.getInt("fotoPerfil")
                );
            }
        } catch (SQLException e) {
            System.err.println("Error al iniciar sesión: " + e);
        }
        return user;
    }

    @Override
    public BeanUsuario miCuenta(int idUsuario) {
        final String qwery = "select * from Usuario where idUsuario=?";
        BeanUsuario usuario = null;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                usuario = new BeanUsuario(
                        rs.getInt("idUsuario"),
                        rs.getString("nombre"),
                        rs.getString("apellidoP"),
                        rs.getString("apellidoM"),
                        rs.getString("correo"),
                        rs.getString("contrasena"),
                        rs.getInt("fotoPerfil")
                );
            }
        } catch (SQLException e) {
            System.err.println("Error para ingresar a cuenta: " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return usuario;
    }

    public boolean updateImagen(int idImagen, int idUser) {
        final String query = "pa_CambiarImagenPerfilUsuario ?,?;";
        status = false;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, idImagen);
            ps.setInt(2, idUser);

            status = ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al modificar la imagen del perfil: " + e);
        }
        return status;
    }

}
