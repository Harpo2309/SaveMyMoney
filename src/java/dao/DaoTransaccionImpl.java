/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanCartera;
import bean.BeanTablaTransferencia;
import bean.BeanTransaccion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import util.ConnectionBD;
import util.interfaces.DaoTransaccion;

/**
 *
 * @author WIN7
 */
public class DaoTransaccionImpl implements DaoTransaccion {

    private Connection con;
    private ResultSet rs;
    private PreparedStatement ps;
    private CallableStatement cs;
    private boolean status;

    @Override
    public boolean add(BeanTransaccion transaccion, int idUsuario) {
        final String qwery = "execute pa_insertarTransferencia ?,?,?,?,?,?";
        boolean flag = false;
        try {
            con = ConnectionBD.getConnection();
            cs = con.prepareCall(qwery);
            cs.setFloat(1, transaccion.getMonto());
            cs.setString(2, transaccion.getFecha());
            cs.setInt(3, transaccion.getUnaCarteraQuita().getIdCartera());
            cs.setInt(4, transaccion.getUnaCarteraRecibe().getIdCartera());
            cs.setInt(5, idUsuario);
            cs.registerOutParameter(6, java.sql.Types.BIT);
            flag = cs.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al insertar cartera: " + e);
        }
        return flag;
    }

    @Override
    public List<BeanTransaccion> consultarTransacciones(int idUsuario) {
        List<BeanTransaccion> transaccionesRealizadas = new ArrayList();
        final String qwery = "EXECUTE pa_mostrarTransferencias ?";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanTransaccion beanTransaccion = new BeanTransaccion();
                beanTransaccion.setIdTransaccion(rs.getInt("idTransferencia"));
                beanTransaccion.setMonto(rs.getFloat("monto"));
                beanTransaccion.setFecha(rs.getString("fecha"));
                BeanCartera beanCarteraQuita = new BeanCartera();
                beanCarteraQuita.setIdCartera(rs.getInt("idCarteraQuita"));
                beanTransaccion.setUnaCarteraQuita(beanCarteraQuita);
                BeanCartera beanCarteraRecibe = new BeanCartera();
                beanCarteraRecibe.setIdCartera(rs.getInt("idCarteraQuita"));
                beanTransaccion.setUnaCarteraRecibe(beanCarteraRecibe);
            }

        } catch (SQLException e) {
            System.err.println("Error al listar carteras: " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return transaccionesRealizadas;
    }
    
    public List<BeanTablaTransferencia> listaTransacciones(int idUsuario){
        final String qwery = "pa_mostrarTransferencias ?";
        List<BeanTablaTransferencia> listaTrans = new ArrayList<>();
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            while(rs.next()){
               BeanTablaTransferencia trans = new BeanTablaTransferencia();
               trans.setFecha(rs.getString("fecha")) ;
               trans.setCarteraOrigen(rs.getString("nombrePon"));
               trans.setMonto(rs.getDouble("monto")); 
               trans.setCarteraDestino(rs.getString("nombreQuita"));
               listaTrans.add(trans);
            }
        } catch (SQLException e) {
            System.out.println("Error al mostrar las transferencias" + e);
        }finally{
            try {
                if (con!=null) {
                    con.close();
                }
                if (ps!=null) {
                    ps.close();
                    
                }
                if (rs!=null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.out.println("Error al cerrar las conexines");
            }
        }
        return listaTrans;
    }

}
