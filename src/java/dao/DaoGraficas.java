/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanGrafica;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import util.ConnectionBD;

/**
 *
 * @author Harpo
 */
public class DaoGraficas {

    private Connection con;
    private ResultSet rs;
    private PreparedStatement ps;
    private CallableStatement cs;
    private boolean status;

    public List<BeanGrafica> datosGrafica(int idUsuario) {
        List<BeanGrafica> datos = new ArrayList();
        final String qwery = "SELECT DISTINCT C.nombre AS nombreCategoria,SUM(G.monto) AS totalGastos,G.idUsuario FROM Gasto AS G JOIN Categoria AS C ON G.idCategoria=C.idCategoria GROUP BY C.idCategoria,G.idUsuario,c.nombre,C.nombre HAVING G.idUsuario=?;";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                datos.add(
                        new BeanGrafica(
                                rs.getString("nombreCategoria"),
                                rs.getInt("totalGastos")
                        ));
            }
            System.out.println("Tamaño de la lista: (grafica): "+datos.size());
        } catch (SQLException e) {
            System.err.println("Error en la lista datosGrafica: "+e);
        }finally{
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return datos;
    }

}
