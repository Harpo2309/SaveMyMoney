/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.BeanAbono;
import bean.BeanCartera;
import bean.BeanTipoCartera;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import util.ConnectionBD;
import util.interfaces.DaoCartera;

/**
 *
 * @author Harpo
 */
public class DaoCarteraImpl implements DaoCartera {

    private Connection con;
    private ResultSet rs;
    private PreparedStatement ps;
    private CallableStatement cs;
    private boolean status;

    @Override
    public boolean add(BeanCartera cartera, int idUsuario) {
        
        final String qwery = "execute pa_registrarCartera ?,?,?,?,?,?";
        boolean flag = false;
        try {
            con = ConnectionBD.getConnection();
            cs = con.prepareCall(qwery);
            cs.setString(1, cartera.getNombre());
            cs.setDouble(2, cartera.getSaldo());
            cs.setDouble(3, cartera.getCreditoMaximo());
            cs.setInt(4, idUsuario);
            cs.setInt(5, cartera.getIdTipoCartera());
            cs.registerOutParameter(6, java.sql.Types.BIT);
            flag = cs.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al insertar cartera: " + e);
        }
        return flag;
    }

    @Override
    public boolean delete(int idCartera) {
        System.out.println("id recibida en dao delete: " + idCartera);
        boolean flag = false;
        final String qwery = "EXECUTE pa_eliminarCartera ?,?";
        try {
            con = ConnectionBD.getConnection();
            cs = con.prepareCall(qwery);
            cs.setInt(1, idCartera);
            cs.registerOutParameter(2, java.sql.Types.BIT);
            cs.executeUpdate();
            flag = cs.getInt(2) == 1;
            System.out.println("valor devuelto: " + cs.getInt(2));
            System.out.println(flag);
        } catch (SQLException e) {
            System.err.println("Error al eliminar: " + e);
        }
        return flag;
    }

    @Override
    public boolean update(BeanCartera cartera) {
        final String qwery = "EXECUTE pa_modificarCartera ?,?,?,?,?";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setString(1, cartera.getNombre());
            ps.setDouble(2, cartera.getSaldo());
            ps.setDouble(3, cartera.getCreditoMaximo());
            ps.setInt(4, cartera.getIdTipoCartera());
            ps.setInt(5, cartera.getIdCartera());

            status = ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al modificar categoria: " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return status;
    }

    

    @Override
    public List<BeanCartera> carterasUsuarioCredito(int idUsuario) {
        List<BeanCartera> carterasCredito = new ArrayList();
        final String qwery = "EXECUTE pa_mostrarCarterasCredito ?";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                carterasCredito.add(
                        new BeanCartera(
                                rs.getInt("idCartera"),
                                rs.getString("nombre"),
                                rs.getDouble("saldo"),
                                rs.getDouble("creditoMaximo"),
                                rs.getInt("idUsuario"),
                                rs.getInt("idTipoCartera")
                        )
                );
            }
            System.out.println("Tamaño de la lista (credito): " + carterasCredito.size());
        } catch (SQLException e) {
            System.err.println("Error al listar carteras: " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return carterasCredito;
    }
    
    
    @Override
    public List<BeanCartera> carterasOUsuarioOtro(int idUsuario) {
        List<BeanCartera> carterasOtro = new ArrayList();
        final String qwery = "EXECUTE pa_mostrarCarterasOtro ?";
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                carterasOtro.add(
                        new BeanCartera(
                                rs.getInt("idCartera"),
                                rs.getString("nombre"),
                                rs.getDouble("saldo"),
                                rs.getDouble("creditoMaximo"),
                                rs.getInt("idUsuario"),
                                rs.getInt("idTipoCartera")
                        )
                );
            }
            System.out.println("Tamaño de la lista (otro): " + carterasOtro.size());
        } catch (SQLException e) {
            System.err.println("Error al listar carteras: " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return carterasOtro;
    }
    
    
    public List<BeanTipoCartera>tiposCartera(){
        final String qwery = "SELECT * FROM tipoCartera;";
        List<BeanTipoCartera> tiposCartera = new ArrayList<>();
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            rs = ps.executeQuery();
            while(rs.next()){
                tiposCartera.add(
                        new BeanTipoCartera(rs.getInt("idTipoCartera"), rs.getString("nombre"))
                );
            }
        } catch (SQLException e) {
            System.err.println("Error al obtener tipos de cartera: " + e);
        }
        return tiposCartera;
    }
    

    public boolean abonarCartera(BeanAbono abono) {
        final String qwery ="EXECUTE pa_insertarIngreso ?,?,?,?,?,?";
        status = false;
        try {
            con = ConnectionBD.getConnection();
            cs = con.prepareCall(qwery);
            cs.setString(1, abono.getDescrip());
            cs.setDouble(2, abono.getMonto());
            cs.setString(3, abono.getFecha());
            cs.setInt(4, abono.getIdCartera());
            cs.setInt(5, abono.getIdUser());
            cs.registerOutParameter(6, java.sql.Types.BIT);
            status = cs.executeUpdate() == 1;
        } catch (SQLException e) {
            System.err.println("Error al abonar a la cartera: " + e);
        }
        return status;
    }
    
    public int totalCarteras(int idUsuario){
        final String qwery = "SELECT SUM(saldo) AS totalCarteras FROM Cartera GROUP BY idUsuario HAVING idUsuario=?;";
        int totalCar = 0;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                totalCar = rs.getInt("totalCarteras");
            }
            System.out.println("total de las carteras del usuario " +idUsuario + ": " + totalCar);
        } catch (SQLException e) {
            System.err.println("Error al mostrar total de carteras: " + e);
        }finally{
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return totalCar;
    }
    
    
    public int totalCarterasCred(int idUsuario){
        final String qwery = "SELECT SUM(saldo) AS totalCarteraCredito FROM Cartera GROUP BY idUsuario,idTipoCartera HAVING idUsuario=? AND idTipoCartera=1;";
        int totalCarCred = 0;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                totalCarCred = rs.getInt("totalCarteraCredito");
            }
            System.out.println("total cart. cred usuario: " +idUsuario + ": " + totalCarCred);
        } catch (SQLException e) {
            System.err.println("Error al mostrar total de carteras: " + e);
        }finally{
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return totalCarCred;
    }
    
    public int totalCarterasOtro(int idUsuario){
        final String qwery = "SELECT SUM(saldo) AS totalCarteraOtro FROM Cartera GROUP BY idUsuario,idTipoCartera HAVING idUsuario=? AND idTipoCartera=2;";
        int totalCarOtro = 0;
        try {
            con = ConnectionBD.getConnection();
            ps = con.prepareStatement(qwery);
            ps.setInt(1, idUsuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                totalCarOtro = rs.getInt("totalCarteraOtro");
            }
            System.out.println("total de las carteras del usuario " +idUsuario + ": " + totalCarOtro);
        } catch (SQLException e) {
            System.err.println("Error al mostrar total de carteras: " + e);
        }finally{
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexiones: " + e);
            }
        }
        return totalCarOtro;
    }

}
