/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *
 * @author Harpo
 */
public class ConnectionBD {

    private static String ipAdd;
    private static String port;
    private static String base;
    private static String user;
    private static String pass;
    private static final ResourceBundle datos = ResourceBundle.getBundle("datosBase");

    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            System.err.println("Error de clase: " + e);
        }
        ipAdd = datos.getString("ipAdd");
        port = datos.getString("port");
        base = datos.getString("base");
        user = datos.getString("user");
        pass = datos.getString("pass");
        
        String url = "jdbc:sqlserver://" + ipAdd + ":" + port + ";databaseName=" + base;
        return DriverManager.getConnection(url,user,pass);
    }
}
