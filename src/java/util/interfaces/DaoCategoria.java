/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.interfaces;

import bean.BeanCategoria;
import java.util.List;

/**
 *
 * @author Alumno
 */
public interface DaoCategoria {

    boolean registrarCategoria(BeanCategoria categoria, int idUsuario);

    List<BeanCategoria> listarCategorias(int idUsuario);

    List<BeanCategoria> listarCategoriasDefecto(int idUsuario);

    boolean eliminarCategoria(int idCategoria);
    
    boolean actualizarCategoria(BeanCategoria categoria);
}
