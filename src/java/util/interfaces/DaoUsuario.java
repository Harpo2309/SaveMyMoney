/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.interfaces;

import bean.BeanUsuario;

/**
 *
 * @author Harpo
 */
public interface DaoUsuario {
    boolean add(BeanUsuario usuario);
    boolean delete(BeanUsuario usuario);
    boolean update(BeanUsuario usuario);
    BeanUsuario buscarUsuario(String correo, String pass);
    BeanUsuario miCuenta(int idUsuario);
}
