/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.interfaces;

import bean.BeanCartera;
import bean.BeanCategoria;
import bean.BeanGasto;
import java.util.List;

/**
 *
 * @author redes1
 */
public interface DaoGasto {
    boolean registrarGasto(BeanGasto gasto,int idUsuario);
    List<BeanCategoria> listarCategorias(int idUsuario);
    List<BeanCartera> listarCarteras(int idUsuario);
    List<BeanGasto> listarGastos(int idUsuario);
    boolean eliminarGasto(int idGasto);
}
