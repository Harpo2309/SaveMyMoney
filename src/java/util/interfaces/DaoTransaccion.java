/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.interfaces;

import bean.BeanTransaccion;
import java.util.List;

/**
 *
 * @author WIN7
 */
public interface DaoTransaccion {
    
    boolean add(BeanTransaccion transaccion, int idUsuario);
     List<BeanTransaccion>consultarTransacciones(int idUsuario);
    
}
