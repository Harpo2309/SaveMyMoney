/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.interfaces;
import bean.BeanCartera;
import java.util.List;
/**
 *
 * @author Harpo
 */
public interface DaoCartera {
    boolean add(BeanCartera cartera, int idUsuario);
    boolean delete(int idCartera);
    boolean update(BeanCartera cartera);
    List<BeanCartera>carterasUsuarioCredito(int idUsuario);
    List<BeanCartera>carterasOUsuarioOtro(int idUsuario);
}
