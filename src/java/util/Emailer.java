/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;



import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Harpo
 */
public class Emailer  {

    private static String from="smmsevices@gmail.com";
    private static String password="savemoney123";
    private String to;
    private String subject="";
    private String body= "";
    static Properties properties = new Properties();

    static {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        
    }


    public String enviarCorreoRegistro(String user,String UserPass) {
        to = user;
        String ret = SUCCESS;
        try {
            Session session = Session.getDefaultInstance(properties,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, password);
                }
            });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            subject = "Save My Money: Registro Exitoso";
            message.setSubject(subject);
            body = "¡Bienvenido a Save My Money!\nLos datos de cuenta son los siguientes:\n"+
            "Correo electrónico: " + user + "\n" +
            "Contraseña: " + UserPass;
            message.setText(body);
            Transport.send(message);
        } catch (Exception e) {
            ret = ERROR;
          
            e.printStackTrace();
        }
        return ret;
    }
    
    
    
    public String enviarCorreoRecContra(String user,String UserPass) {
        to = user;
        String ret = SUCCESS;
        try {
            Session session = Session.getDefaultInstance(properties,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, password);
                }
            });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            subject = "Save My Money: Recuperar contraseña";
            message.setSubject(subject);
            body = "¡Descuida! Podrás seguir usando tu cuenta, tus datos son los siguientes:\n"+
            "Correo electrónico: " + user + "\n" +
            "Contraseña: " + UserPass;
            message.setText(body);
            Transport.send(message);
        } catch (Exception e) {
            ret = ERROR;
          
            e.printStackTrace();
        }
        return ret;
    }
}
