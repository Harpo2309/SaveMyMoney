/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import bean.BeanCartera;
import bean.BeanImagenPerfil;
import bean.BeanUsuario;
import bean.BeanTipoCartera;
import bean.BeanAbono;
import bean.BeanCategoria;
import bean.BeanGasto;
import bean.BeanGrafica;
import bean.BeanTablaTransferencia;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import dao.DaoCarteraImpl;
import dao.DaoCategoriaImpl;
import dao.DaoGastoImpl;
import dao.DaoGraficas;
import dao.DaoImagenPerfil;
import dao.DaoTransaccionImpl;
import dao.DaoUsuarioImpl;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Harpo
 */
public class ControlCartera extends ActionSupport implements SessionAware {

    //Valores del usuario
    private BeanCartera cartera;
    private BeanUsuario usuario;
    private BeanAbono abono;
    private int idCartera;
    private int imagen;
    private int totalCar;
    private int totalCarCred;
    private int totalCarOtro;
    //listas de sus carteras
    private List<BeanCartera> carterasUsuarioCredito;
    private List<BeanCartera> carterasUsuarioOtro;
    private List<BeanCartera> misCarteras;
    private List<BeanTipoCartera> tiposCartera;
    //imágenes de perfil
    List<BeanImagenPerfil> misImagenes;
    //Datos de la gráfica
    private List<BeanGrafica> datosGrafica;
    //Lista de gastos
    private List<BeanGasto> misGastos;
    //Lista de categorías
    private List<BeanCategoria> misCategorias;
    private List<BeanCategoria> categoriasDefecto;
    private List<BeanCategoria> unasCategorias;
    //Lista de transacciones
    private List<BeanTablaTransferencia> misTransacciones;
    //Objetos DAO utilizados
    DaoUsuarioImpl daoUsuario = new DaoUsuarioImpl();
    DaoCarteraImpl daoCartera = new DaoCarteraImpl();
    //Mapa de la sesión
    private Map session;
    //Posible mensaje de error
    private String message;

    public String modificarCartera() {
        if (new DaoCarteraImpl().update(getCartera())) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido modificar la cartera");
            return ERROR;
        }
    }

    public String eliminarCartera() {
        if (new DaoCarteraImpl().delete(idCartera)) {
            return SUCCESS;
        } else {
            setMessage("NO se ha podido eliminar la cartera");
            return ERROR;
        }
    }

    public String crearCartera() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        if (new DaoCarteraImpl().add(cartera, idUsuario)) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido crear la cartera");
            return ERROR;
        }
    }

    public String miCuenta() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        setUsuario(daoUsuario.miCuenta(idUsuario));
        setCarterasUsuarioCredito(daoCartera.carterasUsuarioCredito(idUsuario));
        setCarterasUsuarioOtro(daoCartera.carterasOUsuarioOtro(idUsuario));
        setTiposCartera(new DaoCarteraImpl().tiposCartera());
        setMisImagenes(new DaoImagenPerfil().listaImagen());
        setImagen(new DaoImagenPerfil().imagenPerfil(idUsuario));
        setTotalCar(new DaoCarteraImpl().totalCarteras(getUsuario().getIdUsuario()));
        setTotalCarCred(new DaoCarteraImpl().totalCarterasCred(getUsuario().getIdUsuario()));
        setTotalCarOtro(new DaoCarteraImpl().totalCarterasOtro(getUsuario().getIdUsuario()));
        setMisGastos(new DaoGastoImpl().listarGastos(idUsuario));
        setMisCarteras(new DaoGastoImpl().listarCarteras(getUsuario().getIdUsuario()));
        setMisCategorias(new DaoGastoImpl().listarCategorias(getUsuario().getIdUsuario()));
        setUnasCategorias(new DaoCategoriaImpl().listarCategorias(getUsuario().getIdUsuario()));
        setCategoriasDefecto(new DaoCategoriaImpl().listarCategoriasDefecto(getUsuario().getIdUsuario()));
        //valores para las graficas
        setDatosGrafica(new DaoGraficas().datosGrafica(getUsuario().getIdUsuario()));
        setMisTransacciones(new DaoTransaccionImpl().listaTransacciones(getUsuario().getIdUsuario()));
        return getUsuario() != null ? SUCCESS : ERROR;
    }

    public String abonarCartera() {
        if (new DaoCarteraImpl().abonarCartera(getAbono())) {
            return SUCCESS;
        }else{
            setMessage("No se ha podido realizar el abono a la cartera");
            return ERROR;
        }
    }

    /*
    GETTERS AND SETTERS
     */
    public BeanCartera getCartera() {
        return cartera;
    }

    public void setCartera(BeanCartera cartera) {
        this.cartera = cartera;
    }

    public BeanUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(BeanUsuario usuario) {
        this.usuario = usuario;
    }

    public int getIdCartera() {
        return idCartera;
    }

    public void setIdCartera(int idCartera) {
        this.idCartera = idCartera;
    }

    public List<BeanCartera> getCarterasUsuarioCredito() {
        return carterasUsuarioCredito;
    }

    public void setCarterasUsuarioCredito(List<BeanCartera> carterasUsuarioCredito) {
        this.carterasUsuarioCredito = carterasUsuarioCredito;
    }

    public List<BeanCartera> getCarterasUsuarioOtro() {
        return carterasUsuarioOtro;
    }

    public void setCarterasUsuarioOtro(List<BeanCartera> carterasUsuarioOtro) {
        this.carterasUsuarioOtro = carterasUsuarioOtro;
    }

    public List<BeanTipoCartera> getTiposCartera() {
        return tiposCartera;
    }

    public void setTiposCartera(List<BeanTipoCartera> tiposCartera) {
        this.tiposCartera = tiposCartera;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public List<BeanImagenPerfil> getMisImagenes() {
        return misImagenes;
    }

    public void setMisImagenes(List<BeanImagenPerfil> misImagenes) {
        this.misImagenes = misImagenes;
    }

    public BeanAbono getAbono() {
        return abono;
    }

    public void setAbono(BeanAbono abono) {
        this.abono = abono;
    }

    public List<BeanGrafica> getDatosGrafica() {
        return datosGrafica;
    }

    public void setDatosGrafica(List<BeanGrafica> datosGrafica) {
        this.datosGrafica = datosGrafica;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public List<BeanGasto> getMisGastos() {
        return misGastos;
    }

    public void setMisGastos(List<BeanGasto> misGastos) {
        this.misGastos = misGastos;
    }

    public List<BeanCategoria> getMisCategorias() {
        return misCategorias;
    }

    public void setMisCategorias(List<BeanCategoria> misCategorias) {
        this.misCategorias = misCategorias;
    }

    public List<BeanCartera> getMisCarteras() {
        return misCarteras;
    }

    public void setMisCarteras(List<BeanCartera> misCarteras) {
        this.misCarteras = misCarteras;
    }

    public List<BeanCategoria> getCategoriasDefecto() {
        return categoriasDefecto;
    }

    public void setCategoriasDefecto(List<BeanCategoria> categoriasDefecto) {
        this.categoriasDefecto = categoriasDefecto;
    }

    public List<BeanCategoria> getUnasCategorias() {
        return unasCategorias;
    }

    public void setUnasCategorias(List<BeanCategoria> unasCategorias) {
        this.unasCategorias = unasCategorias;
    }

    public DaoUsuarioImpl getDaoUsuario() {
        return daoUsuario;
    }

    public void setDaoUsuario(DaoUsuarioImpl daoUsuario) {
        this.daoUsuario = daoUsuario;
    }

    public DaoCarteraImpl getDaoCartera() {
        return daoCartera;
    }

    public void setDaoCartera(DaoCarteraImpl daoCartera) {
        this.daoCartera = daoCartera;
    }

    public List<BeanTablaTransferencia> getMisTransacciones() {
        return misTransacciones;
    }

    public void setMisTransacciones(List<BeanTablaTransferencia> misTransacciones) {
        this.misTransacciones = misTransacciones;
    }

    public int getTotalCar() {
        return totalCar;
    }

    public void setTotalCar(int totalCar) {
        this.totalCar = totalCar;
    }

    public int getTotalCarCred() {
        return totalCarCred;
    }

    public void setTotalCarCred(int totalCarCred) {
        this.totalCarCred = totalCarCred;
    }

    public int getTotalCarOtro() {
        return totalCarOtro;
    }

    public void setTotalCarOtro(int totalCarOtro) {
        this.totalCarOtro = totalCarOtro;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
