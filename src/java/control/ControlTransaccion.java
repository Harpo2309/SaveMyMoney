/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import bean.BeanTransaccion;
import com.opensymphony.xwork2.ActionContext;

import com.opensymphony.xwork2.ActionSupport;
import dao.DaoTransaccionImpl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author redes1
 */
public class ControlTransaccion extends ActionSupport {

    private BeanTransaccion unaTransaccion;
    private List<BeanTransaccion> misTransacciones;
    private Map session;
    private String message;
    
    DaoTransaccionImpl daoTransaccion = new DaoTransaccionImpl();
    
    public String registrarTransaccion() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        System.out.println("cartera que envia " + getUnaTransaccion().getUnaCarteraQuita().getIdCartera());
        System.out.println("cartera que recibe " + getUnaTransaccion().getUnaCarteraRecibe().getIdCartera());
        System.out.println("monto" + getUnaTransaccion().getMonto());
        System.out.println("fecha " + getUnaTransaccion().getFecha());
        setMisTransacciones(daoTransaccion.consultarTransacciones(idUsuario));        
        if (new DaoTransaccionImpl().add(unaTransaccion, idUsuario)) {
          return SUCCESS;  
        }else{
            setMessage("No se ha podido realizar la transacción");
            return ERROR;
        }
    }
    
    public BeanTransaccion getUnaTransaccion() {
        return unaTransaccion;
    }
    
    public void setUnaTransaccion(BeanTransaccion unaTransaccion) {
        this.unaTransaccion = unaTransaccion;
    }
    
    public List<BeanTransaccion> getMisTransacciones() {
        return misTransacciones;
    }
    
    public void setMisTransacciones(List<BeanTransaccion> misTransacciones) {
        this.misTransacciones = misTransacciones;
    }
    
    public Map getSession() {
        return session;
    }
    
    public void setSession(Map session) {
        this.session = session;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
