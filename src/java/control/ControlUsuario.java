/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import bean.BeanCartera;
import bean.BeanCategoria;
import bean.BeanGasto;
import bean.BeanGrafica;
import bean.BeanImagenPerfil;
import bean.BeanNuevoUsuario;
import bean.BeanTablaTransferencia;
import bean.BeanTipoCartera;
import bean.BeanUsuario;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import dao.DaoCarteraImpl;
import dao.DaoCategoriaImpl;
import dao.DaoGastoImpl;
import dao.DaoGraficas;
import dao.DaoUsuarioImpl;
import dao.DaoImagenPerfil;
import dao.DaoTransaccionImpl;
import util.Emailer;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Harpo
 */
public class ControlUsuario extends ActionSupport implements SessionAware {
    //Objetos BEAN que permiten la creación de usuarios y la obtención de sus datos
    private BeanUsuario usuario;
    private BeanNuevoUsuario nuevoUsuario = BeanNuevoUsuario.getBeanNuevoUsuario();
    //Imagen de perfil
    private int imagen;
    //Total de las carteras
    private int totalCar;
    private int totalCarCred;
    private int totalCarOtro;
    //Datos de acceso a la cuenta
    private String correo;
    private String password;
    //Lista de carteras
    private List<BeanCartera> carterasUsuarioCredito;
    private List<BeanCartera> carterasUsuarioOtro;
    private List<BeanTipoCartera> tiposCartera;
    //Datos de la gráfica
    private List<BeanGrafica> datosGrafica;
    //Datos de las transacciones
    private List<BeanTablaTransferencia> misTransacciones;
    //Lista de imágenes de perfil
    private List<BeanImagenPerfil> misImagenes;
    //Lista de gastos
    private List<BeanGasto> misGastos;
    //Lista de categorías
    private List<BeanCategoria> misCategorias;
    private List<BeanCategoria> categoriasDefecto;
    private List<BeanCategoria> unasCategorias;
    //Lista de carteras
    private List<BeanCartera> misCarteras;
    //Clase que permite enviar correos electrónicos a los nuevos usuarios registrados
    private final Emailer emailer = new Emailer();
    //Mensaje de bienvenida para los usuarios registrados
    private String mensaje = "";
    //Mensaje de error
    private String message;
    //Mapa de la sesión
    private Map session;
    //Objeto DAO para realizar las transacciones
    DaoTransaccionImpl daoTransaccion = new DaoTransaccionImpl();

    public String loginUsuario() {
        if (!session.containsKey("logged")) {
            try {
                //Datos del usuario
                setUsuario(new DaoUsuarioImpl().buscarUsuario(getCorreo(), getPassword()));
                //Datos de las carteras
                setMisCarteras(new DaoGastoImpl().listarCarteras(getUsuario().getIdUsuario()));
                setCarterasUsuarioCredito(new DaoCarteraImpl().carterasUsuarioCredito(getUsuario().getIdUsuario()));
                setCarterasUsuarioOtro(new DaoCarteraImpl().carterasOUsuarioOtro(getUsuario().getIdUsuario()));
                setTiposCartera(new DaoCarteraImpl().tiposCartera());
                //Lista de imágenes de perfil
                setMisImagenes(new DaoImagenPerfil().listaImagen());
                setImagen(new DaoImagenPerfil().imagenPerfil(getUsuario().getIdUsuario()));
                //Totales de las carteras
                setTotalCar(new DaoCarteraImpl().totalCarteras(getUsuario().getIdUsuario()));
                setTotalCarCred(new DaoCarteraImpl().totalCarterasCred(getUsuario().getIdUsuario()));
                setTotalCarOtro(new DaoCarteraImpl().totalCarterasOtro(getUsuario().getIdUsuario()));
                //valores para las graficas
                setDatosGrafica(new DaoGraficas().datosGrafica(getUsuario().getIdUsuario()));
                //Transacciones del usuario
                setMisTransacciones(new DaoTransaccionImpl().listaTransacciones(getUsuario().getIdUsuario()));
                //Gastos del usuario
                setMisGastos(new DaoGastoImpl().listarGastos(getUsuario().getIdUsuario()));
                //Categorías del usuario
                setMisCategorias(new DaoGastoImpl().listarCategorias(getUsuario().getIdUsuario()));
                setUnasCategorias(new DaoCategoriaImpl().listarCategorias(getUsuario().getIdUsuario()));
                setCategoriasDefecto(new DaoCategoriaImpl().listarCategoriasDefecto(getUsuario().getIdUsuario()));
                session = ActionContext.getContext().getSession();
                session.put("logged", "yes");
                session.put("id", getUsuario().getIdUsuario());
                session.put("name", getUsuario().getNombre());
                session.put("apellido1", getUsuario().getApellidoPaterno());
                session.put("apellido2", getUsuario().getApellidoMaterno());
                session.put("mail", getUsuario().getCorreo());
                session.put("password", getUsuario().getPassword());
                session.put("img", getImagen());
                return SUCCESS;
            } catch (NullPointerException e) {
                mensaje = "Correo y/o contraseña incorrectos.";                
                return ERROR;
            }
        } else {
            try {
                session = ActionContext.getContext().getSession();
                String mail = session.get("mail").toString();
                String pass = session.get("password").toString();
                int id = Integer.parseInt(session.get("id").toString());

                setUsuario(new DaoUsuarioImpl().buscarUsuario(mail, pass));
                setCarterasUsuarioCredito(new DaoCarteraImpl().carterasUsuarioCredito(id));
                setCarterasUsuarioOtro(new DaoCarteraImpl().carterasOUsuarioOtro(id));
                setTiposCartera(new DaoCarteraImpl().tiposCartera());
                setMisImagenes(new DaoImagenPerfil().listaImagen());
                setTotalCar(new DaoCarteraImpl().totalCarteras(getUsuario().getIdUsuario()));
                setTotalCarCred(new DaoCarteraImpl().totalCarterasCred(getUsuario().getIdUsuario()));
                setTotalCarOtro(new DaoCarteraImpl().totalCarterasOtro(getUsuario().getIdUsuario()));
                setImagen(new DaoImagenPerfil().imagenPerfil(id));
                setMisGastos(new DaoGastoImpl().listarGastos(id));
                setMisCarteras(new DaoGastoImpl().listarCarteras(getUsuario().getIdUsuario()));
                setMisCategorias(new DaoGastoImpl().listarCategorias(getUsuario().getIdUsuario()));
                setUnasCategorias(new DaoCategoriaImpl().listarCategorias(getUsuario().getIdUsuario()));
                setCategoriasDefecto(new DaoCategoriaImpl().listarCategoriasDefecto(getUsuario().getIdUsuario()));
                //valores para las graficas
                setDatosGrafica(new DaoGraficas().datosGrafica(id));
                setMisTransacciones(new DaoTransaccionImpl().listaTransacciones(getUsuario().getIdUsuario()));
                return SUCCESS;
            } catch (Exception e) {
                mensaje = "Correo y/o contraseña incorrectos.";                
                return ERROR;
            }
        }

    }

    public String cerrarSesion() {
        session = ActionContext.getContext().getSession();
        session.clear();
        return SUCCESS;
    }

    public String inicioCrearCuenta() {
        if (getNuevoUsuario().getNombre() != null
                && getNuevoUsuario().getApellidoMaterno() != null
                && getNuevoUsuario().getApellidoPaterno() != null) {
            setMisImagenes(new DaoImagenPerfil().listaImagen());
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    public String crearCuenta() {
        boolean flag = false;
        String mail;
        String pass;
        if (getNuevoUsuario().getCorreo() != null && getNuevoUsuario().getPassword() != null) {
            setUsuario(getNuevoUsuario());
            mail = getUsuario().getCorreo();
            pass = getUsuario().getPassword();
            flag = new DaoUsuarioImpl().add(getUsuario());
            setUsuario(new DaoUsuarioImpl().buscarUsuario(mail, pass));
            setCarterasUsuarioCredito(new DaoCarteraImpl().carterasUsuarioCredito(getUsuario().getIdUsuario()));
            setCarterasUsuarioOtro(new DaoCarteraImpl().carterasOUsuarioOtro(getUsuario().getIdUsuario()));
            setTotalCar(new DaoCarteraImpl().totalCarteras(getUsuario().getIdUsuario()));
            setTotalCarCred(new DaoCarteraImpl().totalCarterasCred(getUsuario().getIdUsuario()));
            setTotalCarOtro(new DaoCarteraImpl().totalCarterasOtro(getUsuario().getIdUsuario()));
            //valores para las graficas
            setDatosGrafica(new DaoGraficas().datosGrafica(getUsuario().getIdUsuario()));
            setMisTransacciones(new DaoTransaccionImpl().listaTransacciones(getUsuario().getIdUsuario()));
            //emailer.enviarCorreoRegistro(mail, pass);
            mensaje = "¡Tu cuenta ha sido creada!\nInicia sesión para continuar";
        }
        return flag == true && getCarterasUsuarioCredito() != null && getCarterasUsuarioOtro() != null ? SUCCESS : ERROR;
    }

    public String modificarPerfil() {
        try {
            session = ActionContext.getContext().getSession();
            session.put("id", getUsuario().getIdUsuario());
            session.put("name", getUsuario().getNombre());
            session.put("apellido1", getUsuario().getApellidoPaterno());
            session.put("apellido2", getUsuario().getApellidoMaterno());
            session.put("mail", getUsuario().getCorreo());
            session.put("password", getUsuario().getPassword());

            if (new DaoUsuarioImpl().update(getUsuario())) {
                return SUCCESS;
            }else{
                setMessage("No se ha podido modificar tu perfil");
                return ERROR;
            }
        } catch (Exception e) {
            System.err.println("Error al modificar perfil: " + e);
            setMessage("No se ha podido modificar tu perfil");
            return ERROR;
        }
    }

    public String cambiarImagenPerfil() {
        try {
            session = ActionContext.getContext().getSession();
            int idUser = Integer.parseInt(session.get("id").toString());
            int idImg = getImagen();
            System.out.println("El idUser= " + idUser + "El idImg= " + idImg);
            DaoUsuarioImpl dao = new DaoUsuarioImpl();
            if (dao.updateImagen(idImg, idUser)) {
                setImagen(new DaoImagenPerfil().imagenPerfil(idUser));
                session.put("img", getImagen());
                return SUCCESS;
            } else {
                setMessage("No se ha podido cambiar tu avatar");
                return ERROR;
            }
        } catch (Exception e) {
            System.err.println("Error al modificar imagen de perfil: " + e);
            setMessage("No se ha podido cambiar tu avatar");
            return ERROR;
        }
    }

    public String misTransacciones() {
        try {
            session = ActionContext.getContext().getSession();
            int idUser = Integer.parseInt(session.get("id").toString());
            DaoTransaccionImpl daoTrans = new DaoTransaccionImpl();
            setMisTransacciones(daoTrans.listaTransacciones(idUser));
            return SUCCESS;
        } catch (Exception e) {
            System.out.println("Error en la lista de transacciones " + e);
            setMessage("No se han podido mostrar tus transacciones");
            return ERROR;
        }
    }

    /*
    GETTERS AND SETTERS
     */
    public BeanUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(BeanUsuario usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<BeanCartera> getCarterasUsuarioCredito() {
        return carterasUsuarioCredito;
    }

    public void setCarterasUsuarioCredito(List<BeanCartera> carterasUsuarioCredito) {
        this.carterasUsuarioCredito = carterasUsuarioCredito;
    }

    public List<BeanCartera> getCarterasUsuarioOtro() {
        return carterasUsuarioOtro;
    }

    public void setCarterasUsuarioOtro(List<BeanCartera> carterasUsuarioOtro) {
        this.carterasUsuarioOtro = carterasUsuarioOtro;
    }

    public BeanNuevoUsuario getNuevoUsuario() {
        return nuevoUsuario;
    }

    public void setNuevoUsuario(BeanNuevoUsuario nuevoUsuario) {
        this.nuevoUsuario = nuevoUsuario;
    }

    public List<BeanTipoCartera> getTiposCartera() {
        return tiposCartera;
    }

    public void setTiposCartera(List<BeanTipoCartera> tiposCartera) {
        this.tiposCartera = tiposCartera;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }

    public Map getSession() {
        return session;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<BeanImagenPerfil> getMisImagenes() {
        return misImagenes;
    }

    public void setMisImagenes(List<BeanImagenPerfil> misImagenes) {
        this.misImagenes = misImagenes;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public List<BeanGrafica> getDatosGrafica() {
        return datosGrafica;
    }

    public void setDatosGrafica(List<BeanGrafica> datosGrafica) {
        this.datosGrafica = datosGrafica;
    }

    public List<BeanTablaTransferencia> getMisTransacciones() {
        return misTransacciones;
    }

    public void setMisTransacciones(List<BeanTablaTransferencia> misTransacciones) {
        this.misTransacciones = misTransacciones;
    }

    public List<BeanGasto> getMisGastos() {
        return misGastos;
    }

    public void setMisGastos(List<BeanGasto> misGastos) {
        this.misGastos = misGastos;
    }

    public List<BeanCategoria> getMisCategorias() {
        return misCategorias;
    }

    public void setMisCategorias(List<BeanCategoria> misCategorias) {
        this.misCategorias = misCategorias;
    }

    public List<BeanCartera> getMisCarteras() {
        return misCarteras;
    }

    public void setMisCarteras(List<BeanCartera> misCarteras) {
        this.misCarteras = misCarteras;
    }

    public List<BeanCategoria> getCategoriasDefecto() {
        return categoriasDefecto;
    }

    public void setCategoriasDefecto(List<BeanCategoria> categoriasDefecto) {
        this.categoriasDefecto = categoriasDefecto;
    }

    public List<BeanCategoria> getUnasCategorias() {
        return unasCategorias;
    }

    public void setUnasCategorias(List<BeanCategoria> unasCategorias) {
        this.unasCategorias = unasCategorias;
    }

    public DaoTransaccionImpl getDaoTransaccion() {
        return daoTransaccion;
    }

    public void setDaoTransaccion(DaoTransaccionImpl daoTransaccion) {
        this.daoTransaccion = daoTransaccion;
    }

    public int getTotalCar() {
        return totalCar;
    }

    public void setTotalCar(int totalCar) {
        this.totalCar = totalCar;
    }

    public int getTotalCarCred() {
        return totalCarCred;
    }

    public void setTotalCarCred(int totalCarCred) {
        this.totalCarCred = totalCarCred;
    }

    public int getTotalCarOtro() {
        return totalCarOtro;
    }

    public void setTotalCarOtro(int totalCarOtro) {
        this.totalCarOtro = totalCarOtro;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
}
