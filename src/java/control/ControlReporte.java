/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import util.ConnectionBD;

/**
 *
 * @author Harpo
 */
public class ControlReporte extends ActionSupport implements SessionAware{

    Connection con;
    Map<String, Object> params;
    List gastos;
    List abonos;
    List transacciones;
    private Map session;
    
    public String reporteGastos() {
        try {
            con = ConnectionBD.getConnection();
             
        } catch (SQLException e) {
            System.err.println("Error de conexion al reporte de gastos: " + e);
        }
        session = ActionContext.getContext().getSession(); 
        int id = Integer.parseInt(session.get("id").toString());
        System.out.println("Id usuario que pide reporte= " + id);
        gastos = new ArrayList();
        params = new HashMap<>();
        params.put("idUser", id);
        params.put("RUTA", "http://localhost:8080/SaveMyMoney");
        return SUCCESS;
    }

    public String reporteAbonos() {
        try {
            con = ConnectionBD.getConnection();
        } catch (SQLException e) {
            System.err.println("Error de conexion al reporte: " + e);
        }
        session = ActionContext.getContext().getSession(); 
        int id = Integer.parseInt(session.get("id").toString());
        System.out.println("Id usuario que pide reporte= " + id);
        abonos = new ArrayList();
        params = new HashMap<>();
        params.put("idUser", id);
        params.put("RUTA", "http://localhost:8080/SaveMyMoney");
        return SUCCESS;
    }
    
    public String reporteTransacciones() {
        try {
            con = ConnectionBD.getConnection();
        } catch (SQLException e) {
            System.err.println("Error de conexion al reporte: " + e);
        }
        session = ActionContext.getContext().getSession(); 
        int id = Integer.parseInt(session.get("id").toString());
        System.out.println("Id usuario que pide reporte= " + id);
        transacciones = new ArrayList();
        params = new HashMap<>();
        params.put("idUser", id);
        params.put("RUTA", "http://localhost:8080/SaveMyMoney");
        return SUCCESS;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public List getGastos() {
        return gastos;
    }

    public void setGastos(List gastos) {
        this.gastos = gastos;
    }

    public List getAbonos() {
        return abonos;
    }

    public void setAbonos(List abonos) {
        this.abonos = abonos;
    }

    public List getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(List transacciones) {
        this.transacciones = transacciones;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }
    
    
    
}
