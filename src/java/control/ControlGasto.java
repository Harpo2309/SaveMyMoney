/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import bean.BeanAbono;
import bean.BeanCartera;
import bean.BeanCategoria;
import bean.BeanGasto;
import bean.BeanImagenPerfil;
import bean.BeanTipoCartera;
import bean.BeanUsuario;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import dao.DaoCarteraImpl;
import dao.DaoGastoImpl;
import dao.DaoImagenPerfil;
import dao.DaoUsuarioImpl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author User-1
 */
public class ControlGasto {

    private BeanCartera cartera;
    private BeanUsuario usuario;
    private BeanAbono abono;
    private int idCartera;
    private int idGasto;
    private int imagen;
    private List<BeanCartera> carterasUsuarioCredito;
    private List<BeanCartera> carterasUsuarioOtro;
    private List<BeanTipoCartera> tiposCartera;
    private List<BeanImagenPerfil> misImagenes;
    private String message;

    DaoUsuarioImpl daoUsuario = new DaoUsuarioImpl();
    DaoCarteraImpl daoCartera = new DaoCarteraImpl();
    DaoGastoImpl daoGasto = new DaoGastoImpl();

    private BeanGasto unGasto;
    private List<BeanCategoria> misCategorias;
    private List<BeanCartera> misCarteras;
    private List<BeanGasto> misGastos;
    private Map session;

//    public String mostrarGastos() {
//        session = ActionContext.getContext().getSession();
//        int idUsuario = Integer.parseInt(session.get("id").toString());
//        DaoGastoImpl daoGasto = new DaoGastoImpl();
//        setMisGastos(daoGasto.listarGastos(idUsuario));
//        return SUCCESS;
//    }
    public String miCuenta() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        setUsuario(daoUsuario.miCuenta(idUsuario));
        setCarterasUsuarioCredito(daoCartera.carterasUsuarioCredito(idUsuario));
        setCarterasUsuarioOtro(daoCartera.carterasOUsuarioOtro(idUsuario));
        setTiposCartera(new DaoCarteraImpl().tiposCartera());
        setMisImagenes(new DaoImagenPerfil().listaImagen());
        setImagen(new DaoImagenPerfil().imagenPerfil(idUsuario));
        setMisGastos(daoGasto.listarGastos(idUsuario));
        return getUsuario() != null ? SUCCESS : ERROR;
    }

    public String registrarGasto() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        BeanGasto beanGasto = new BeanGasto();
        beanGasto.setDescripcion(getUnGasto().getDescripcion());
        beanGasto.setMonto(getUnGasto().getMonto());
        beanGasto.setFecha(getUnGasto().getFecha());
        BeanCategoria beanCategoria = new BeanCategoria();
        beanCategoria.setIdCategoria(getUnGasto().getCategoria().getIdCategoria());
        beanGasto.setCategoria(beanCategoria);
        BeanCartera beanCartera = new BeanCartera();
        beanCartera.setIdCartera(getUnGasto().getCartera().getIdCartera());
        beanGasto.setCartera(beanCartera);
        DaoGastoImpl daoGasto = new DaoGastoImpl();
        if (daoGasto.registrarGasto(beanGasto, idUsuario)) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido registrar el gasto");
            return ERROR;
        }
    }

    public String eliminarGasto() {
        DaoGastoImpl midao = new DaoGastoImpl();
        if (midao.eliminarGasto(getIdGasto())) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido eliminar el gasto");
            return ERROR;
        }
    }

    public BeanGasto getUnGasto() {
        return unGasto;
    }

    public void setUnGasto(BeanGasto unGasto) {
        this.unGasto = unGasto;
    }

    public List<BeanCategoria> getMisCategorias() {
        return misCategorias;
    }

    public void setMisCategorias(List<BeanCategoria> misCategorias) {
        this.misCategorias = misCategorias;
    }

    public List<BeanCartera> getMisCarteras() {
        return misCarteras;
    }

    public void setMisCarteras(List<BeanCartera> misCarteras) {
        this.misCarteras = misCarteras;
    }

    public ControlGasto() {
    }

    public Map getSession() {
        return session;
    }

    public List<BeanGasto> getMisGastos() {
        return misGastos;
    }

    public void setMisGastos(List<BeanGasto> misGastos) {
        this.misGastos = misGastos;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public BeanCartera getCartera() {
        return cartera;
    }

    public void setCartera(BeanCartera cartera) {
        this.cartera = cartera;
    }

    public BeanUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(BeanUsuario usuario) {
        this.usuario = usuario;
    }

    public BeanAbono getAbono() {
        return abono;
    }

    public void setAbono(BeanAbono abono) {
        this.abono = abono;
    }

    public int getIdCartera() {
        return idCartera;
    }

    public void setIdCartera(int idCartera) {
        this.idCartera = idCartera;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public List<BeanCartera> getCarterasUsuarioCredito() {
        return carterasUsuarioCredito;
    }

    public void setCarterasUsuarioCredito(List<BeanCartera> carterasUsuarioCredito) {
        this.carterasUsuarioCredito = carterasUsuarioCredito;
    }

    public List<BeanCartera> getCarterasUsuarioOtro() {
        return carterasUsuarioOtro;
    }

    public void setCarterasUsuarioOtro(List<BeanCartera> carterasUsuarioOtro) {
        this.carterasUsuarioOtro = carterasUsuarioOtro;
    }

    public List<BeanTipoCartera> getTiposCartera() {
        return tiposCartera;
    }

    public void setTiposCartera(List<BeanTipoCartera> tiposCartera) {
        this.tiposCartera = tiposCartera;
    }

    public List<BeanImagenPerfil> getMisImagenes() {
        return misImagenes;
    }

    public void setMisImagenes(List<BeanImagenPerfil> misImagenes) {
        this.misImagenes = misImagenes;
    }

    public DaoUsuarioImpl getDaoUsuario() {
        return daoUsuario;
    }

    public void setDaoUsuario(DaoUsuarioImpl daoUsuario) {
        this.daoUsuario = daoUsuario;
    }

    public DaoCarteraImpl getDaoCartera() {
        return daoCartera;
    }

    public void setDaoCartera(DaoCarteraImpl daoCartera) {
        this.daoCartera = daoCartera;
    }

    public DaoGastoImpl getDaoGasto() {
        return daoGasto;
    }

    public void setDaoGasto(DaoGastoImpl daoGasto) {
        this.daoGasto = daoGasto;
    }

    public int getIdGasto() {
        return idGasto;
    }

    public void setIdGasto(int idGasto) {
        this.idGasto = idGasto;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    

}
