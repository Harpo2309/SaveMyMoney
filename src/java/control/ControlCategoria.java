/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import bean.BeanCategoria;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import dao.DaoCategoriaImpl;
import java.util.Map;

/**
 *
 * @author Alumno
 */
public class ControlCategoria {

    private Map session;
    private BeanCategoria unaCategoria;
    private int idCategoria;
    private String nombre;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public BeanCategoria getUnaCategoria() {
        return unaCategoria;
    }

    public void setUnaCategoria(BeanCategoria unaCategoria) {
        this.unaCategoria = unaCategoria;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String registrarCategoria() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        DaoCategoriaImpl daoC = new DaoCategoriaImpl();
        BeanCategoria beanC = new BeanCategoria();
        beanC.setNombre(getUnaCategoria().getNombre());
        if (daoC.registrarCategoria(beanC, idUsuario)) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido crear la categoría");
            return ERROR;
        }
    }

    public String eliminarCategoria() {
        session = ActionContext.getContext().getSession();
        int idUsuario = Integer.parseInt(session.get("id").toString());
        DaoCategoriaImpl daoC = new DaoCategoriaImpl();
        if (daoC.eliminarCategoria(getIdCategoria())) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido eliminar la categoría");
            return ERROR;
        }
    }

    public String modificarCategoria() {
        DaoCategoriaImpl midao = new DaoCategoriaImpl();
        BeanCategoria beanCategoria = new BeanCategoria();
        beanCategoria.setIdCategoria(getIdCategoria());
        beanCategoria.setNombre(getNombre());
        if (midao.actualizarCategoria(beanCategoria)) {
            return SUCCESS;
        } else {
            setMessage("No se ha podido modificar la categoría");
            return ERROR;
        }
    }
}
