/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Celeste
 */
public class BeanImagenPerfil {
    private int idImagen;
    private String nombre;

    public BeanImagenPerfil() {
    }

    
    public BeanImagenPerfil(int idImagen, String nombre) {
        this.idImagen = idImagen;
        this.nombre = nombre;
    }

    public BeanImagenPerfil(String nombre) {
        this.nombre = nombre;
    }

    public int getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
