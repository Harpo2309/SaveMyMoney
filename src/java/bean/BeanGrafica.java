/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Harpo
 */
public class BeanGrafica {
    private String nombreCategoria;
    private int totalGastos;
    
    private int idUsuario;

    public BeanGrafica(String nombreCategoria, int totalGastos) {
        this.nombreCategoria = nombreCategoria;
        this.totalGastos = totalGastos;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public int getTotalGastos() {
        return totalGastos;
    }

    public void setTotalGastos(int totalGastos) {
        this.totalGastos = totalGastos;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    

    
    public int getGastos() {
        return totalGastos;
    }

    public void setGastos(int gastos) {
        this.totalGastos = gastos;
    }

    public String getCategoria() {
        return nombreCategoria;
    }

    public void setCategoria(String categoria) {
        this.nombreCategoria = categoria;
    }
    
}
