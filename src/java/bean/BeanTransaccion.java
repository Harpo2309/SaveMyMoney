/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author redes1
 */
public class BeanTransaccion {

    private int idTransaccion;
    private float monto;
    private String fecha;
    private BeanCartera unaCarteraQuita;
    private BeanCartera unaCarteraRecibe;
    private BeanUsuario unUsuario;

    public BeanTransaccion() {
    }

    public BeanTransaccion(int idTransaccion, float monto, String fecha, BeanCartera unaCarteraQuita, BeanCartera unaCarteraRecibe, BeanUsuario unUsuario) {
        this.idTransaccion = idTransaccion;
        this.monto = monto;
        this.fecha = fecha;
        this.unaCarteraQuita = unaCarteraQuita;
        this.unaCarteraRecibe = unaCarteraRecibe;
        this.unUsuario = unUsuario;
    }

    public int getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(int idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public BeanCartera getUnaCarteraQuita() {
        return unaCarteraQuita;
    }

    public void setUnaCarteraQuita(BeanCartera unaCarteraQuita) {
        this.unaCarteraQuita = unaCarteraQuita;
    }

    public BeanCartera getUnaCarteraRecibe() {
        return unaCarteraRecibe;
    }

    public void setUnaCarteraRecibe(BeanCartera unaCarteraRecibe) {
        this.unaCarteraRecibe = unaCarteraRecibe;
    }

    public BeanUsuario getUnUsuario() {
        return unUsuario;
    }

    public void setUnUsuario(BeanUsuario unUsuario) {
        this.unUsuario = unUsuario;
    }

    
    
}
