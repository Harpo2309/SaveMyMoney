/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Harpo
 */
public class BeanAbono {
    private int idUser;
    private int idCartera;
    private double monto;
    private String descrip;
    private String fecha;

    public BeanAbono() {
    }

    public BeanAbono(int idUser, int idCartera, double monto, String descrip, String fecha) {
        this.idUser = idUser;
        this.idCartera = idCartera;
        this.monto = monto;
        this.descrip = descrip;
        this.fecha = fecha;
    }

    

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdCartera() {
        return idCartera;
    }

    public void setIdCartera(int idCartera) {
        this.idCartera = idCartera;
    }
    
    
}
