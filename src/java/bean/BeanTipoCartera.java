/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Harpo
 */
public class BeanTipoCartera {
    private int idTipoCartera;
    private String nombre;

    public BeanTipoCartera() {
    }

    public BeanTipoCartera(int idTipoCartera, String nombre) {
        this.idTipoCartera = idTipoCartera;
        this.nombre = nombre;
    }

    public int getIdTipoCartera() {
        return idTipoCartera;
    }

    public void setIdTipoCartera(int idTipoCartera) {
        this.idTipoCartera = idTipoCartera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
