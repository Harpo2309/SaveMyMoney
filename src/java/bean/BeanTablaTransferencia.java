/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Harpo
 */
public class BeanTablaTransferencia {
    private String fecha;
     private String carteraOrigen;
    private double monto;
    private String carteraDestino;

    public BeanTablaTransferencia() {
    }

    public BeanTablaTransferencia(String fecha, String carteraOrigen, double monto, String carteraDestino) {
        this.fecha = fecha;
        this.carteraOrigen = carteraOrigen;
        this.monto = monto;
        this.carteraDestino = carteraDestino;
    }

   

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getCarteraOrigen() {
        return carteraOrigen;
    }

    public void setCarteraOrigen(String carteraOrigen) {
        this.carteraOrigen = carteraOrigen;
    }

    public String getCarteraDestino() {
        return carteraDestino;
    }

    public void setCarteraDestino(String carteraDestino) {
        this.carteraDestino = carteraDestino;
    }
    
    
}
