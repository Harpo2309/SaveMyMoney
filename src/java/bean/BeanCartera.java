/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Harpo
 */
public class BeanCartera {
    private int idCartera;
    private String nombre;
    private double saldo;
    private double creditoMaximo;
    private int idUsuario;
    private int idTipoCartera;

    public BeanCartera() {
    }

    public BeanCartera(int idCartera, String nombre, double saldo, double creditoMaximo, int idUsuario, int idTipoCartera) {
        this.idCartera = idCartera;
        this.nombre = nombre;
        this.saldo = saldo;
        this.creditoMaximo = creditoMaximo;
        this.idUsuario = idUsuario;
        this.idTipoCartera = idTipoCartera;
    }
    
    

   

    

    public BeanCartera(String nombre, float saldo) {
        this.nombre = nombre;
        this.saldo = saldo;
    }

    
    
    

    public int getIdCartera() {
        return idCartera;
    }

    public void setIdCartera(int idCartera) {
        this.idCartera = idCartera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public double getCreditoMaximo() {
        return creditoMaximo;
    }

    public void setCreditoMaximo(double creditoMaximo) {
        this.creditoMaximo = creditoMaximo;
    }

    public int getIdTipoCartera() {
        return idTipoCartera;
    }

    public void setIdTipoCartera(int idTipoCartera) {
        this.idTipoCartera = idTipoCartera;
    }

    
    
    
    
}
